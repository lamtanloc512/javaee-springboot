import { Row, Col, Card, Form, Button } from 'react-bootstrap';

const CrudTable = () => {
  return (
    <div className="content">
      <Row>
        <Col xs="12">
          <Card className="card-default">
            <Card.Header>
              <Card.Title as="h3">CRUD Voucher</Card.Title>
              <div className="card-tools">
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="collapse"
                >
                  <i className="fas fa-minus" />
                </button>
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="remove"
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </Card.Header>
            <Card.Body>
              <Form.Group className="row form-group">
                <Col md={2}>
                  <Form.Label>Mã Voucher</Form.Label>
                </Col>
                <Col md={10}>
                  <Form.Control
                    type="text"
                    name="vouchercodes"
                    placeholder="Voucher code"
                    className="form-control"
                  />
                </Col>
              </Form.Group>
              <Form.Group className="row form-group">
                <Col md={2}>
                  <Form.Label>Phần trăm giảm giá</Form.Label>
                </Col>
                <Col md={10}>
                  <Form.Control
                    type="number"
                    id="input-discount"
                    name="discount"
                    placeholder="Discount"
                    className="form-control"
                  />
                </Col>
              </Form.Group>
              <Form.Group className="row form-group">
                <Col md={2}>
                  <Form.Label>Ghi chú</Form.Label>
                </Col>
                <Col md={10}>
                  <Form.Control
                    type="text"
                    id="input-note"
                    name="note"
                    placeholder="Note"
                    className="form-control"
                  />
                </Col>
              </Form.Group>
              <Form.Group className="row form-group">
                <Col md={2}>
                  <Form.Label>Ngày tạo</Form.Label>
                </Col>
                <Col md={10}>
                  <Form.Control
                    type="date"
                    id="input-createDate"
                    name="createDate"
                    placeholder="Create Date"
                    className="form-control"
                  />
                </Col>
              </Form.Group>
              <Form.Group className="row form-group">
                <Col md={2}>
                  <Form.Label>Ngày cập nhật</Form.Label>
                </Col>
                <Col md={10}>
                  <Form.Control
                    type="date"
                    id="input-updateDate"
                    name="updateDate"
                    placeholder="Update Date"
                    className="form-control"
                  />
                </Col>
              </Form.Group>
            </Card.Body>
            <Card.Footer className="bg-white">
              <Row>
                <Col sm={6}>
                  <Button id="save_data" variant="info">
                    Save Data
                  </Button>
                </Col>
                <Col sm={6} className="d-flex justify-content-end">
                  <Button
                    className="btn-delete-all"
                    id="btn_delete_all"
                    variant="danger"
                  >
                    Save Data
                  </Button>
                </Col>
              </Row>
            </Card.Footer>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default CrudTable;
