import MainSideBar from '../components/MainSideBar';
import Navbar from '../components/Navbar';
function Layout({ children }) {
  return (
    <>
      <Navbar />
      <MainSideBar />
      <main>{children}</main>
    </>
  );
}

export default Layout;
