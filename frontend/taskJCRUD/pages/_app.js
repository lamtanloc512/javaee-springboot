import Layout from '../components/Layout';
import Head from 'next/head';
import Script from 'next/script';
function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>AdminLTE</title>
        <meta name="description" content="AdminLTE" />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="/plugins/fontawesome-free/css/all.min.css"
        />
        <link rel="stylesheet" href="/dist/css/adminlte.min.css" />
      </Head>
      <Layout>
        <Component {...pageProps} />
        <Script
          src="/plugins/jquery/jquery.min.js"
          strategy="beforeInteractive"
        />
        <Script
          src="/plugins/bootstrap/js/bootstrap.bundle.min.js"
          strategy="afterInteractive"
        />
        <Script src="/dist/js/adminlte.min.js" strategy="afterInteractive" />
      </Layout>
    </>
  );
}

export default MyApp;
