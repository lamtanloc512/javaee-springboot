import Head from 'next/head';
import Script from 'next/script';
import Link from 'next/link';
import DataTable from 'react-data-table-component';
import CrudTable from '../components/voucher/CrudTable';
import { useState, useEffect } from 'react';

const Voucher = () => {
  const vColumns = [
    {
      name: 'STT',
      selector: (row) => row.stt,
    },
    {
      name: 'Mã giảm giá',
      selector: (row) => row.maVoucher,
    },
    {
      name: 'Phần trăm giảm giá',
      selector: (row) => row.phanTramGiamGia,
    },
    {
      name: 'Ghi chú',
      selector: (row) => row.ghiChu,
    },
    {
      name: 'Ngày tạo',
      selector: (row) => row.ngayTao,
    },
    {
      name: 'Ngày cập nhật',
      selector: (row) => row.ngayCapNhat,
    },
    {
      name: 'Actions',
      cell: (row) => (
        <button
          className="btn-delete btn btn-danger btn-sm"
          onClick={() => {
            handleDeleteVoucher(row);
          }}
          id={row.id}
        >
          Delete
        </button>
      ),
      selector: (row) => row.actions,
      button: true,
    },
  ];

  const [vData, setvData] = useState([]);
  const [vIsRefesh, setvIsRefesh] = useState(false);

  const getAllVouchers = async () => {
    await fetch('http://localhost:8080/api/vouchers').then((paramRespone) => {
      const vRespone = paramRespone.json();
      vRespone.then((value) => {
        let vSTT = 1;
        value.map((bI) => (bI.stt = vSTT++));
        setvData([...value]);
      });
    });
  };

  const handleDeleteVoucher = async (paramvData) => {
    let vConfirm = confirm('Are you sure to delete this voucher?');
    if (vConfirm) {
      const vRequest = await fetch(
        `http://localhost:8080/api/vouchers/${paramvData.id}`,
        {
          method: 'DELETE',
        }
      );
      const vRespone = await vRequest;
      console.log(vRespone);
      if (vRespone.ok) {
        alert('Deleted!');
        setvIsRefesh(!vIsRefesh);
      } else {
        alert('Something went wrong!');
      }
    }
  };

  useEffect(() => {
    getAllVouchers();
  }, [vIsRefesh]);
  return (
    <>
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Vouchers Manager</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <Link href="/">
                      <a>Home</a>
                    </Link>
                  </li>
                  <li className="breadcrumb-item active">Voucher</li>
                </ol>
              </div>
            </div>
            <CrudTable />
            <div className="row">
              <div className="col-12">
                <div className="card card-default">
                  <div className="card-body">
                    <DataTable columns={vColumns} data={vData} pagination />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Script
        src="/plugins/jquery/jquery.min.js"
        strategy="beforeInteractive"
      />
      <Script
        src="/plugins/bootstrap/js/bootstrap.bundle.min.js"
        strategy="afterInteractive"
      />
      <Script src="/dist/js/adminlte.min.js" strategy="afterInteractive" />
    </>
  );
};
export default Voucher;
