/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-is-valid  */

import { Link } from 'react-router-dom';

const Sidebar = () => {
  return (
    <>
      {/* Main Sidebar Container */}
      <aside className="main-sidebar sidebar-dark-primary elevation-4">
        {/* Brand Logo */}
        <a href="!#" className="brand-link">
          <img
            src="../../dist/img/AdminLTELogo.png"
            alt="AdminLTE Logo"
            className="brand-image img-circle elevation-3"
            style={{ opacity: '.8' }}
          />
          <span className="brand-text font-weight-light">Admin</span>
        </a>
        {/* Sidebar */}
        <div className="sidebar">
          {/* Sidebar user (optional) */}
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img
                src="../../dist/img/user2-160x160.jpg"
                className="img-circle elevation-2"
                alt="User Image"
              />
            </div>
            <div className="info">
              <a href="!#" className="d-block">
                loclt@devcamp.edu.vn
              </a>
            </div>
          </div>
          {/* SidebarSearch Form */}
          <div className="form-inline">
            <div className="input-group" data-widget="sidebar-search">
              <input
                className="form-control form-control-sidebar"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <div className="input-group-append">
                <button className="btn btn-sidebar">
                  <i className="fas fa-search fa-fw" />
                </button>
              </div>
            </div>
          </div>
          {/* Sidebar Menu */}
          <nav className="mt-2">
            <ul
              className="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              {/* Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library */}
              <li className="nav-item menu-open">
                <span className="nav-link active" style={{ cursor: 'pointer' }}>
                  <i className="nav-icon fas fa-tachometer-alt" />
                  <p>
                    Dashboard
                    <i className="right fas fa-angle-left" />
                  </p>
                </span>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Income</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a className="nav-link" style={{ cursor: 'pointer' }}>
                  <i className="fas fa-scroll nav-icon" />
                  <p>
                    Voucher
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/voucher/list" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Voucher list</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a className="nav-link" style={{ cursor: 'pointer' }}>
                  <i className="fas fa-scroll nav-icon" />
                  <p>
                    Order
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/order/list" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Orders list</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="order/detail" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Order detail</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a className="nav-link" style={{ cursor: 'pointer' }}>
                  <i className="fas fa-cocktail nav-icon" />
                  <p>
                    Drinks
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/order-list" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Orders list</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/order-detail" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Order detail</p>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a className="nav-link" style={{ cursor: 'pointer' }}>
                  <i className="fas fa-bars nav-icon" />
                  <p>
                    Combos
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/order-list" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Orders list</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/order-detail" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Order detail</p>
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
          {/* /.sidebar-msenu */}
        </div>
        {/* /.sidebar */}
      </aside>
    </>
  );
};

export default Sidebar;
