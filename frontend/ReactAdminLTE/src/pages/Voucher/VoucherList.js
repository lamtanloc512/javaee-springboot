/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-is-valid  */
// import 'datatables.net-bs4';
// import 'datatables.net-responsive';
// import 'datatables.net-responsive-bs4';
import './voucher.css';
import $ from 'jquery';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import ReactDOM from 'react-dom';
import { VoucherEditModal } from './VoucherEditModal';
import { VoucherCreateNewModal } from './VoucherCreateNewModal';
import { VoucherDeleteModal } from './VoucherDeleteModal';
import { useSelector, useDispatch } from 'react-redux';
import { fetchAllVouchers } from '../../redux/voucher/voucherSlice';

const VoucherList = ({}) => {
  const voucher = useSelector((state) => state.voucher.vouchers);
  const dispatch = useDispatch();

  const [showAdd, setShowAdd] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [vRowData, setvRowData] = useState({});
  const handleAddVoucherButtonShow = () => setShowAdd(true);
  const handleEditButtonShow = () => setShowEdit(true);
  const handleDeleteButtonShow = () => setShowDelete(true);

  //add new button
  const onAddVoucherButtonClick = (e) => {
    handleAddVoucherButtonShow();
  };
  // edit button
  const onEditButtonClick = (e) => {
    let vTable = $('#table-orders').DataTable();
    let vRowData = vTable.row($(e.target).parents('tr')).data();
    setvRowData({ ...vRowData });
    handleEditButtonShow();
    vTable.state.clear();
  };

  // delete button
  const onDeleteButtonClick = (e) => {
    let vTable = $('#table-orders').DataTable();
    let vRowData = vTable.row($(e.target).parents('tr')).data();
    setvRowData({ ...vRowData });
    handleDeleteButtonShow();
  };

  const createDataTable = (paramData) => {
    const vColumns = [
      'id',
      'maVoucher',
      'phanTramGiamGia',
      'ghiChu',
      'ngayTao',
      'ngayCapNhat',
    ];

    let vTable = $('#table-orders').DataTable({
      retrieve: true,
      paging: true,
      lengthChange: true,
      searching: true,
      ordering: true,
      info: true,
      autoWidth: false,
      responsive: true,
      columns: [
        { data: vColumns[0] },
        { data: vColumns[1] },
        { data: vColumns[2] },
        { data: vColumns[3] },
        { data: vColumns[4] },
        { data: vColumns[5] },
        { data: vColumns[6] },
      ],
      columnDefs: [
        {
          targets: 0,
          searchable: false,
          orderable: false,
        },
        {
          targets: 6,
          data: null,
          className: 'text-center px-0 mx-0',
          createdCell: (td) => {
            ReactDOM.render(
              <>
                <a
                  onClick={onEditButtonClick}
                  className="btn-view-order btn btn-primary btn-sm text-bold mx-1"
                >
                  <i className="fas fa-pencil-alt mr-1" />
                  Edit
                </a>
                <a
                  onClick={onDeleteButtonClick}
                  className="btn btn-danger btn-sm text-bold mx-1"
                >
                  <i className="fas fa-trash mr-1" />
                  Delete
                </a>
              </>,
              td
            );
          },
        },
      ],
    });

    $('#table-orders tbdy').empty();
    vTable.clear();
    vTable.rows.add(paramData);
    vTable
      .on('order.dt search.dt', function () {
        vTable
          .column(0, { search: 'applied', order: 'applied' })
          .nodes()
          .each(function (cell, i) {
            cell.innerHTML = i + 1;
          });
      })
      .draw();
  };

  useEffect(() => {
    if (Object.keys(voucher).length === 0) {
      dispatch(fetchAllVouchers());
    }
  }, []);

  useEffect(() => {
    if (Object.keys(voucher).length > 0) {
      createDataTable(voucher);
    }
  }, [voucher]);

  return (
    <>
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Voucher</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item">
                  <Link to="/">Home</Link>
                </li>
                <li className="breadcrumb-item active">Voucher list</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-header">
                <Button
                  className="text-bold"
                  variant="success"
                  onClick={onAddVoucherButtonClick}
                >
                  <i className="fas fa-plus mx-1"></i>
                  Add Voucher
                </Button>
                <div className="card-tools">
                  <button
                    type="button"
                    className="btn btn-tool"
                    data-card-widget="collapse"
                    title="Collapse"
                  >
                    <i className="fas fa-minus" />
                  </button>
                  <button
                    type="button"
                    className="btn btn-tool"
                    data-card-widget="remove"
                    title="Remove"
                  >
                    <i className="fas fa-times" />
                  </button>
                </div>
              </div>
              <div className="card-body">
                <table
                  id="table-orders"
                  className="table table-bordered table-hover table-responsive-md"
                >
                  <thead>
                    <tr>
                      <th>Stt</th>
                      <th>Ma voucher</th>
                      <th>Phan tram giam gia</th>
                      <th>Ghi chu</th>
                      <th>Ngay tao</th>
                      <th>Ngay cap nhat</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
      <VoucherEditModal
        data={vRowData}
        show={showEdit}
        onHide={() => setShowEdit(false)}
        onExit={() => {
          dispatch(fetchAllVouchers());
        }}
      />
      <VoucherCreateNewModal
        show={showAdd}
        onHide={() => setShowAdd(false)}
        onExit={() => {
          dispatch(fetchAllVouchers());
        }}
      />
      <VoucherDeleteModal
        data={vRowData}
        show={showDelete}
        onHide={() => setShowDelete(false)}
        onExit={() => {
          dispatch(fetchAllVouchers());
        }}
      />
    </>
  );
};
export default VoucherList;
