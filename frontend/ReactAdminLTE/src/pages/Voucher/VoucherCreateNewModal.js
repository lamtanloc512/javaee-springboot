import { Modal, Button, Row, Col } from 'react-bootstrap';
import $ from 'jquery';
export const VoucherCreateNewModal = (props) => {
  const onCreateButtonClick = (e) => {
    e.preventDefault();
    console.log('hello');
    let vDataSend = {
      maVoucher: $('#inp-new-voucher').val(),
      ghiChu: $('#inp-new-ghichu').val(),
      phanTramGiamGia: $('#inp-new-percent').val(),
    };
    createVoucher(vDataSend);
  };

  const createVoucher = (paramDataSend) => {
    $.ajax({
      type: 'POST',
      url: `http://localhost:8080/api/vouchers/`,
      data: JSON.stringify(paramDataSend),
      contentType: 'application/json;charset=UTF-8',
      success: function (response) {
        alert('Succesfully updated!');
        console.log(response);
      },
      error: function (err) {
        console.log(err.responseText);
      },
    });
  };

  return (
    <form>
      <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header className="bg-success text-white" closeButton>
          <Modal.Title>Create Voucher</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col>
              <div className="form-group">
                <label htmlFor="inputClientCompany">Mã giảm giá</label>
                <input
                  type="text"
                  id="inp-new-voucher"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label htmlFor="inputProjectLeader">
                  Phần trăm giảm giá (%)
                </label>
                <input
                  type="text"
                  id="inp-new-percent"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label htmlFor="inputProjectLeader">Ghi chú</label>
                <input
                  type="text"
                  id="inp-new-ghichu"
                  className="form-control"
                />
              </div>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button
            className="text-bold"
            variant="success"
            onClick={onCreateButtonClick}
          >
            Create
          </Button>
          <Button className="text-bold" variant="danger" onClick={props.onHide}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </form>
  );
};
