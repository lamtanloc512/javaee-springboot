import { Modal, Button, Row, Col } from 'react-bootstrap';
import $ from 'jquery';
export const VoucherDeleteModal = (props) => {
  const dataRow = { ...props.data };

  const onButtonDeleteClick = (e) => {
    e.preventDefault();
    deleteVoucher(dataRow);
  };

  const deleteVoucher = (paramDataRow) => {
    const vId = paramDataRow.id;
    $.ajax({
      type: 'DELETE',
      url: `http://localhost:8080/api/vouchers/${vId}`,
      success: function (response) {
        alert('Succesfully updated!');
      },
      error: function (err) {
        console.log(err.responseText);
      },
    });
  };

  return (
    <form>
      <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header className="bg-danger text-white" closeButton>
          <Modal.Title>Create Voucher</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col className="text-center text-bold">
              <h4> Are you really want to delete this voucher ?</h4>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button
            className="text-bold"
            variant="danger"
            onClick={onButtonDeleteClick}
          >
            Delete
          </Button>
          <Button className="text-bold" variant="dark" onClick={props.onHide}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </form>
  );
};
