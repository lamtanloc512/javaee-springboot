import { VoucherProvider } from './VoucherProvider';
import VoucherList from './VoucherList';

const Voucher = () => {
  return (
    <VoucherProvider>
      <VoucherList />
    </VoucherProvider>
  );
};

export default Voucher;
