import { Modal, Button, Row, Col } from 'react-bootstrap';
import $ from 'jquery';
import { useState, useEffect } from 'react';
export const VoucherEditModal = (props) => {
  const dataRow = { ...props.data };
  const [newData, setNewData] = useState({});

  useEffect(() => {
    if (Object.keys(newData).length > 0) {
      updateVoucher(newData);
    }
  }, [newData]);

  const handleUpdate = (e) => {
    e.preventDefault();
    setNewData({
      ...dataRow,
      maVoucher: $('#inp-voucher').val(),
      ghiChu: $('#inp-ghichu').val(),
      phanTramGiamGia: $('#inp-percent').val(),
    });
  };

  const updateVoucher = (paramNewData) => {
    const vDataSend = { ...paramNewData };
    $.ajax({
      type: 'PUT',
      url: `http://localhost:8080/api/vouchers/${vDataSend.id}`,
      data: JSON.stringify(vDataSend),
      contentType: 'application/json;charset=UTF-8',
      success: function (response) {
        alert('Succesfully updated!');
      },
      error: function (err) {
        console.log(err);
      },
    });
  };

  return (
    <form action="">
      <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header className="bg-primary text-white" closeButton>
          <Modal.Title>Edit Voucher</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col>
              <div className="form-group">
                <label htmlFor="inputName">Voucher Id</label>
                <input
                  type="text"
                  id="inp-id"
                  className="form-control"
                  defaultValue={dataRow.id}
                  disabled={'disabled'}
                />
              </div>
              <div className="form-group">
                <label htmlFor="inputClientCompany">Mã giảm giá</label>
                <input
                  type="text"
                  id="inp-voucher"
                  className="form-control"
                  defaultValue={dataRow.maVoucher}
                />
              </div>
              <div className="form-group">
                <label htmlFor="inputProjectLeader">Phần trăm giảm giá</label>
                <input
                  type="text"
                  id="inp-percent"
                  className="form-control"
                  defaultValue={dataRow.phanTramGiamGia}
                />
              </div>
            </Col>
            <Col>
              <div className="form-group">
                <label htmlFor="inputName">Ngày tạo</label>
                <input
                  type="text"
                  id="inp-ngaytao"
                  className="form-control"
                  defaultValue={dataRow.ngayTao}
                  disabled={'disabled'}
                />
              </div>
              <div className="form-group">
                <label htmlFor="inputClientCompany">Ngày cập nhật</label>
                <input
                  type="text"
                  id="inp-ngaycapnhat"
                  className="form-control"
                  defaultValue={dataRow.ngayCapNhat}
                  disabled={'disabled'}
                />
              </div>
              <div className="form-group">
                <label htmlFor="inputProjectLeader">Ghi chú</label>
                <input
                  type="text"
                  id="inp-ghichu"
                  className="form-control"
                  defaultValue={dataRow.ghiChu}
                />
              </div>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button
            className="text-bold"
            variant="success"
            onClick={handleUpdate}
          >
            Update
          </Button>
          <Button className="text-bold" variant="danger" onClick={props.onHide}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </form>
  );
};
