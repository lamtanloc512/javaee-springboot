import { Outlet } from 'react-router-dom';

export const VoucherProvider = ({ children }) => {
  return (
    <>
      {children}
      <Outlet />
    </>
  );
};
