import OrderDetail from './OrderDetail';
import OrderList from './OrderList';
import { OrderProvider } from './OrderProvider';

export const Order = () => {
  return (
    <OrderProvider>
      <OrderList />
      <OrderDetail />
    </OrderProvider>
  );
};
