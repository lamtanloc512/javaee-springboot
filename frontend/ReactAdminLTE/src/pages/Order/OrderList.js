/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-is-valid  */
import 'datatables.net-bs4';
import 'datatables.net-responsive';
import 'datatables.net-responsive-bs4';
import './order.css';
import $ from 'jquery';
import { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { OrderContext } from './OrderProvider';
import { Modal, Button } from 'react-bootstrap';

const OrderList = () => {
  const [orderPizza, setOrderPizza] = useContext(OrderContext);
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  useEffect(() => {
    const $ = require('jquery');
    let vStt = 1;
    let vSttCol = 0;
    const vActionCol = 6;
    const vColumns = [
      'id',
      'maVoucher',
      'phanTramGiamGia',
      'ghiChu',
      'ngayTao',
      'ngayCapNhat',
    ];

    let vTableOrder = $('#table-orders').DataTable({
      data: orderPizza,
      responsive: true,
      columns: [
        { data: vColumns[vSttCol] },
        { data: vColumns[1] },
        { data: vColumns[2] },
        { data: vColumns[3] },
        { data: vColumns[4] },
        { data: vColumns[5] },
        { data: vActionCol },
      ],
      columnDefs: [
        {
          targets: vSttCol,
          render: function () {
            return vStt++;
          },
        },
        {
          targets: vActionCol,
          className: 'text-center px-0 mx-0',
          defaultContent: `
            <a class="btn-view-order btn btn-primary btn-sm text-bold">
              <i class="fas fa-pencil-alt">
              </i>
              Edit
            </a>  
            <a class="btn btn-danger btn-sm text-bold">
              <i class="fas fa-trash">
              </i>
              Delete
            </a>
            `,
        },
      ],
    });
    return () => {
      vTableOrder.destroy();
    };
  }, [orderPizza]);

  useEffect(() => {
    let vTable = $('#table-orders').DataTable();
    vTable.on('click', '.btn-view-order', function () {
      let vRowSeleted = $(this).closest('tr');
      let vDataRowSelected = vTable.row(vRowSeleted).data();
      handleShow();
    });
  }, []);

  return (
    <>
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Orders</h1>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-sm-right">
                <li className="breadcrumb-item">
                  <Link to="/">Home</Link>
                </li>
                <li className="breadcrumb-item active">OrderList</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-header">
                <h3 className="card-title">Pizza365 Orders</h3>
                <div className="card-tools">
                  <button
                    type="button"
                    className="btn btn-tool"
                    data-card-widget="collapse"
                    title="Collapse"
                  >
                    <i className="fas fa-minus" />
                  </button>
                  <button
                    type="button"
                    className="btn btn-tool"
                    data-card-widget="remove"
                    title="Remove"
                  >
                    <i className="fas fa-times" />
                  </button>
                </div>
              </div>
              <div className="card-body">
                <table
                  id="table-orders"
                  className="table table-bordered table-hover"
                >
                  <thead>
                    <tr>
                      <th>Stt</th>
                      <th>Ma voucher</th>
                      <th>Phan tram giam gia</th>
                      <th>Ghi chu</th>
                      <th>Ngay tao</th>
                      <th>Ngay cap nhat</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
      <DetailModal show={show} onHide={() => setShow(false)} />
    </>
  );
};

export const DetailModal = (props) => {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="bg-primary text-white" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Modal heading
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="form-group">
          <label htmlFor="inputName">Project Name</label>
          <input
            type="text"
            id="inputName"
            className="form-control"
            defaultValue="AdminLTE"
          />
        </div>
        <div className="form-group">
          <label htmlFor="inputStatus">Status</label>
          <select id="inputStatus" className="form-control custom-select">
            <option disabled>Select one</option>
            <option>On Hold</option>
            <option>Canceled</option>
            <option selected>Success</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="inputClientCompany">Client Company</label>
          <input
            type="text"
            id="inputClientCompany"
            className="form-control"
            defaultValue="Deveint Inc"
          />
        </div>
        <div className="form-group">
          <label htmlFor="inputProjectLeader">Project Leader</label>
          <input
            type="text"
            id="inputProjectLeader"
            className="form-control"
            defaultValue="Tony Chicken"
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default OrderList;
