import { useState, createContext } from 'react';
import { Outlet } from 'react-router-dom';
import './order.css';

export const OrderContext = createContext();

export const OrderProvider = ({ children }) => {
  const [orderPizza, setOrderPizza] = useState([
    {
      id: 75,
      maVoucher: '1231212',
      phanTramGiamGia: '2',
      ghiChu: '',
      ngayTao: '05-01-2022',
      ngayCapNhat: null,
    },
    {
      id: 76,
      maVoucher: '12312121',
      phanTramGiamGia: '2',
      ghiChu: '',
      ngayTao: '05-01-2022',
      ngayCapNhat: null,
    },
    {
      id: 77,
      maVoucher: '123121212',
      phanTramGiamGia: '2',
      ghiChu: '',
      ngayTao: '05-01-2022',
      ngayCapNhat: null,
    },
    {
      id: 78,
      maVoucher: '1231212121',
      phanTramGiamGia: '2',
      ghiChu: '',
      ngayTao: '05-01-2022',
      ngayCapNhat: null,
    },
    {
      id: 79,
      maVoucher: '12312121212',
      phanTramGiamGia: '2',
      ghiChu: '',
      ngayTao: '05-01-2022',
      ngayCapNhat: null,
    },
    {
      id: 80,
      maVoucher: '123121212121',
      phanTramGiamGia: '2',
      ghiChu: '',
      ngayTao: '05-01-2022',
      ngayCapNhat: null,
    },
    {
      id: 81,
      maVoucher: '1231212121212',
      phanTramGiamGia: '2',
      ghiChu: '',
      ngayTao: '05-01-2022',
      ngayCapNhat: null,
    },
  ]);
  return (
    <OrderContext.Provider value={[orderPizza, setOrderPizza]}>
      {children}
      <Outlet />
    </OrderContext.Provider>
  );
};
