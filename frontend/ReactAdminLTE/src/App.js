import Layout from './components/Layout/Layout';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomePage from './pages/Home/HomePage';
import OrderDetail from './pages/Order/OrderDetail';
import OrderList from './pages/Order/OrderList';
import { OrderProvider } from './pages/Order/OrderProvider';
import { VoucherProvider } from './pages/Voucher/VoucherProvider';
import VoucherList from './pages/Voucher/VoucherList';

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="order/*" element={<OrderProvider />}>
            <Route path="list" element={<OrderList />} />
            <Route path="detail" element={<OrderDetail />} />
          </Route>
          <Route path="voucher/*" element={<VoucherProvider />}>
            <Route path="list" element={<VoucherList />} />
          </Route>
        </Routes>
      </Layout>
    </Router>
  );
}

export default App;
