import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import $ from 'jquery';

export const fetchAllVouchers = createAsyncThunk('vouchers/all', async () => {
  const respone = await $.ajax({
    type: 'GET',
    url: `http://localhost:8080/api/vouchers/`,
    success: function (response) {
      return response;
    },
    error: function (err) {
      console.log(err.responseText);
    },
  });

  // const request = await fetch(`http://localhost:8080/api/vouchers/`);
  // const respone = await request;
  return respone;
});

export const voucherSlice = createSlice({
  name: 'voucher',
  initialState: {
    vouchers: [],
  },
  reducers: {
    updateVoucherState: (state, action) => {
      console.log(action);

      return {
        ...state,
        vouchers: state.vouchers.map((bI) =>
          bI.id === action.payload.id ? { ...action.payload } : bI
        ),
      };
      // return (state.vouchers = []);
    },
  },
  extraReducers: {
    [fetchAllVouchers.fulfilled]: (state, action) => {
      state.vouchers = action.payload;
    },
  },
});

export const { updateVoucherState } = voucherSlice.actions;

export default voucherSlice.reducer;
