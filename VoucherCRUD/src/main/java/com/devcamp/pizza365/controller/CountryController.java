package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@PostMapping("/country/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newRole = new CCountry();
			newRole.setCountryName(cCountry.getCountryName());
			newRole.setCountryCode(cCountry.getCountryCode());
			newRole.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/country/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			/*
			 * newCountry.setCountryName(cCountry.getCountryName());
			 * newCountry.setCountryCode(cCountry.getCountryCode());
			 * newCountry.getRegions().clear();
			 * newCountry.getRegions().addAll(cCountry.getRegions());
			 * //newCountry.setRegions(cCountry.getRegions());
			 */
			cCountry.setId(newCountry.getId());
			CCountry savedCountry = countryRepository.save(cCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/country/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/country/details/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/country/all")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}

	@Autowired
	private RegionRepository regionRepository;

	@GetMapping("/regions/{countryCode}")
	public ResponseEntity<List<CRegion>> getRegionsByCountryCode(@PathVariable("countryCode") String countryCode) {
		try {
			CCountry vCountry = countryRepository.findByCountryCode(countryCode);

			if (vCountry != null) {
				return new ResponseEntity<>(vCountry.getRegions(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/country/{countryId}/regions")
	public List<CRegion> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		return regionRepository.findByCountryId(countryId);
	}

	@GetMapping("/country/{countryId}/regions/{id}")
	public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,
			@PathVariable(value = "id") Long regionId) {
		return regionRepository.findByIdAndCountryId(regionId, countryId);
	}

	@GetMapping("/region/details/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/region/all")
	public List<CRegion> getAllRegions() {
		return regionRepository.findAll();
	}

	@GetMapping("/country/{countryCode}/{regionCode}")
	public Optional<CRegion> getRegionByCountryCodeAndRegionCode(
			@PathVariable(value = "countryCode") String countryCode,
			@PathVariable(value = "regionCode") String regionCode) {
		Optional<CRegion> optional = regionRepository.findByRegionCodeAndCountryCountryCode(regionCode, countryCode);
		if (optional.isPresent()) {
			return optional;
		} else {
			return null;
		}
	}

	@GetMapping("/region/code/{regionCode}")
	public CRegion getRegionByCode(@PathVariable(value = "regionCode") String regionCode) {
		return regionRepository.findByRegionCode(regionCode);
	}

	@GetMapping("/test/{countryName}")
	public ResponseEntity<Object> getRegionsByCountryname(@PathVariable("countryName") String countryName) {
		try {

			List<CCountry> vCountry = countryRepository.findCountryByCountryNameLike(countryName,
					PageRequest.of(0, 10));
			return new ResponseEntity<>(vCountry, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getCause().getCause().getCause() + ".....");
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

}
