package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.model.CCountry;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);

//	@Query(value = "FROM CCountry WHERE country_name like %?1")
//	List<CCountry> findCountryByCountryNameLike(String countryName, Pageable pageable);
//	@Query(value = "FROM CCountry WHERE country_name like ?1%")
//	List<CCountry> findCountryByCountryNameLike(String countryName, Pageable pageable);
	@Query(value = "FROM CCountry WHERE country_name like %?1%")
	List<CCountry> findCountryByCountryNameLike(String countryName, Pageable pageable);

}
