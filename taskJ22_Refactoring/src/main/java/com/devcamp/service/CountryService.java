package com.devcamp.service;

import java.util.List;

import com.devcamp.model.CountryDTO;

public interface CountryService {
	List<CountryDTO> getAllCountry();

	void getCountryById(Long id);

	void getCountryByCountryCode(String countryCode);

	void getCountryByCountryName(String countryCode);

	void getCountryByRegionName(String regionName);

	void createCountry(CountryDTO countryDTO);

	void updateCountry(CountryDTO countryDTO);

	void deleteCountryById(Long id);

}
