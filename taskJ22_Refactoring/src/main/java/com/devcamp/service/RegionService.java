package com.devcamp.service;

public interface RegionService {
	void getAllRegion();

	void getRegionByRegionCode();

	void getCountryByRegionName();

	void getRegionsByCountry();

	void createRegion();

	void updateRegion();

	void deleteRegionById();

	void countRegionByCountryId();

	void checkRegionById();
}
