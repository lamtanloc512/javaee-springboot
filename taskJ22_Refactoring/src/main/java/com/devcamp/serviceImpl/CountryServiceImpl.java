/**
 * 
 */
package com.devcamp.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.model.CountryDTO;
import com.devcamp.repository.CountryRepository;
import com.devcamp.repository.RegionRepository;
import com.devcamp.service.CountryService;

/**
 * @author lamta
 *
 */
@Service
@Transactional
public class CountryServiceImpl implements CountryService {

	@Autowired
	CountryRepository countryRepository;

	@Autowired
	RegionRepository regionRepository;

	@Override
	public List<CountryDTO> getAllCountry() {
		List<CountryDTO> _countryList = new ArrayList<CountryDTO>();
		countryRepository.findAll().forEach(countryEntity -> {
			_countryList.add(countryEntity.toCountryDTO(countryEntity));
		});
		return _countryList;
	}

	@Override
	public void getCountryByCountryCode(String countryCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getCountryByCountryName(String countryCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getCountryByRegionName(String regionName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createCountry(CountryDTO countryDTO) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCountry(CountryDTO countryDTO) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCountryById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getCountryById(Long id) {
		// TODO Auto-generated method stub

	}

}
