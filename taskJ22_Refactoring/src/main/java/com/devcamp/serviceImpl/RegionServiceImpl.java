/**
 * 
 */
package com.devcamp.serviceImpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.repository.RegionRepository;
import com.devcamp.service.RegionService;

/**
 * @author lamta
 *
 */
@Service
@Transactional
public class RegionServiceImpl implements RegionService {

	@Autowired
	private RegionRepository regionRepository;

	@Override
	public void getAllRegion() {

	}

	@Override
	public void getRegionByRegionCode() {
		// TODO Auto-generated method stub

	}

	@Override
	public void getCountryByRegionName() {
		// TODO Auto-generated method stub

	}

	@Override
	public void getRegionsByCountry() {
		// TODO Auto-generated method stub

	}

	@Override
	public void createRegion() {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateRegion() {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteRegionById() {
		// TODO Auto-generated method stub

	}

	@Override
	public void countRegionByCountryId() {
		// TODO Auto-generated method stub

	}

	@Override
	public void checkRegionById() {
		// TODO Auto-generated method stub

	}

}
