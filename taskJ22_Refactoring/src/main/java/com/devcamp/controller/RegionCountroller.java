package com.devcamp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.entity.CountryEntity;
import com.devcamp.entity.RegionEntity;
import com.devcamp.repository.CountryRepository;
import com.devcamp.repository.RegionRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)

public class RegionCountroller {
	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@GetMapping("/region/all")
	public List<RegionEntity> getAllRegion() {
		return regionRepository.findAll();
	}

	@GetMapping(path = "/region/code")
	public ResponseEntity<Object> getRegionByRegionCode(
			@RequestParam(name = "code", required = true) String regionCode) {
		Optional<RegionEntity> _regionFound = regionRepository.findByRegionCode(regionCode);
		if (_regionFound.isPresent()) {
			return new ResponseEntity<Object>(_regionFound.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(path = "search/country/region")
	public ResponseEntity<Object> getCountryByRegionName(
			@RequestParam(name = "name", required = true) String regionName) {
		Optional<RegionEntity> _regionFound = regionRepository.findByRegionName(regionName);
		if (_regionFound.isPresent()) {
			Optional<CountryEntity> _countryFound = countryRepository
					.findByCountryName(_regionFound.get().getCountryName());

			List<Optional<CountryEntity>> _listCountries = new ArrayList<Optional<CountryEntity>>();

			_listCountries.add(_countryFound);
			return new ResponseEntity<Object>(_countryFound.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/country/{countryId}/regions")
	public List<RegionEntity> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		return regionRepository.findByCountryId(countryId);
	}

	@PostMapping("/region/create/{id}")
	public ResponseEntity<Object> createRegion(@PathVariable("id") Long id, @RequestBody RegionEntity Region) {
		try {
			Optional<CountryEntity> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				RegionEntity newRegion = new RegionEntity();
				newRegion.setRegionName(Region.getRegionName());
				newRegion.setRegionCode(Region.getRegionCode());
				newRegion.setCountry(countryData.get());
				newRegion.setCountryName(countryData.get().getCountryName());

				RegionEntity savedRegion = regionRepository.save(newRegion);
				return new ResponseEntity<>(savedRegion, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified region: " + e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PutMapping("/region/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody RegionEntity Region) {
		Optional<RegionEntity> regionData = regionRepository.findById(id);
		if (regionData.isPresent()) {
			RegionEntity newRegion = regionData.get();
			newRegion.setRegionName(Region.getRegionName());
			newRegion.setRegionCode(Region.getRegionCode());
			RegionEntity savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/region/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/region/count/by")
	public ResponseEntity<Object> countRegionByCountryId(
			@RequestParam(name = "country", required = true) Long countryId) {
		List<RegionEntity> regionList = regionRepository.findByCountryId(countryId);

		Map<String, Long> _objectToApi = new HashMap<String, Long>();

		if (regionList.size() > 0) {
			_objectToApi.put("Số lượng regions by this country", (long) regionList.size());
			return new ResponseEntity<Object>(_objectToApi, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(path = "/region/check/by")
	public ResponseEntity<Object> checkRegionById(@RequestParam(name = "id", required = true) Long regionId) {
		Optional<RegionEntity> region = regionRepository.findById(regionId);

		if (region.isPresent()) {
			return new ResponseEntity<Object>("Exist", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Not exist", HttpStatus.NOT_FOUND);
		}
	}
}
