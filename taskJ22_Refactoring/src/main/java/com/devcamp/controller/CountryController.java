package com.devcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.entity.CountryEntity;
import com.devcamp.model.CountryDTO;
import com.devcamp.repository.CountryRepository;
import com.devcamp.service.CountryService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@GetMapping(path = "/country/code")
	public ResponseEntity<Object> getCountryByCountryCode(
			@RequestParam(name = "code", required = true) String countryCode) {
		Optional<CountryEntity> _countryFound = countryRepository.findByCountryCode(countryCode);
		if (_countryFound.isPresent()) {
			return new ResponseEntity<Object>(_countryFound.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(path = "/country/name")
	public ResponseEntity<Object> getCountryByCountryName(
			@RequestParam(name = "name", required = true) String countryName) {
		Optional<CountryEntity> _countryFound = countryRepository.findByCountryName(countryName);
		if (_countryFound.isPresent()) {
			return new ResponseEntity<Object>(_countryFound.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/country/create")
	public ResponseEntity<Object> createCountry(@RequestBody CountryEntity Country) {
		try {
			CountryEntity newCountry = new CountryEntity();
			newCountry.setCountryName(Country.getCountryName());
			newCountry.setCountryCode(Country.getCountryCode());
			newCountry.setRegions(Country.getRegions());
			CountryEntity savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Country: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/country/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CountryEntity Country) {
		Optional<CountryEntity> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CountryEntity newCountry = countryData.get();
			newCountry.setCountryName(Country.getCountryName());
			newCountry.setCountryCode(Country.getCountryCode());
			newCountry.setRegions(Country.getRegions());
			CountryEntity savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/country/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/details/{id}")
	public CountryEntity getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@Autowired
	CountryService countryService;

	@GetMapping("/country/all")
	public List<CountryDTO> getAllCountry() {
		return countryService.getAllCountry();
	}

}
