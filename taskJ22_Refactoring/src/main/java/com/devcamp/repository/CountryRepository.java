package com.devcamp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.entity.CountryEntity;


public interface CountryRepository extends JpaRepository<CountryEntity, Long>  {
	Optional<CountryEntity> findByCountryCode(String countryCode);
	Optional<CountryEntity> findByCountryName(String countryName);
}
