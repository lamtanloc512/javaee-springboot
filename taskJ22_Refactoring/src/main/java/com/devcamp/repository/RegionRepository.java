package com.devcamp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.entity.RegionEntity;

public interface RegionRepository extends JpaRepository<RegionEntity, Long> {
	List<RegionEntity> findByCountryId(Long countryId);

	Optional<RegionEntity> findByIdAndCountryId(Long id, Long instructorId);

	Optional<RegionEntity> findByRegionCode(String regionCode);

	Optional<RegionEntity> findByRegionName(String regionName);
}
