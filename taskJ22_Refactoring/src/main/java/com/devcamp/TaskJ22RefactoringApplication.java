package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskJ22RefactoringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskJ22RefactoringApplication.class, args);
	}

}
