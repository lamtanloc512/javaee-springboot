package com.devcamp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.devcamp.model.CountryDTO;
import com.devcamp.model.RegionDTO;

@Entity
@Table(name = "dev_country")
public class CountryEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "country_code", unique = true)
	private String countryCode;

	@Column(name = "country_name")
	private String countryName;

	@OneToMany(targetEntity = RegionEntity.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "country_id")
	private List<RegionEntity> regions;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public List<RegionEntity> getRegions() {
		return regions;
	}

	public void setRegions(List<RegionEntity> regions) {
		this.regions = regions;
	}

	public CountryDTO toCountryDTO(CountryEntity countryEntity) {
		CountryDTO countryDTO = new CountryDTO();
		List<RegionDTO> countryDTOList = new ArrayList<>();
		countryDTO.setId(countryEntity.getId());
		countryDTO.setCountryCode(countryEntity.getCountryCode());
		countryDTO.setCountryName(countryEntity.getCountryName());
		countryDTOList.addAll(countryDTOList);
		countryDTO.setRegions(countryDTOList);
		return countryDTO;
	}
}
