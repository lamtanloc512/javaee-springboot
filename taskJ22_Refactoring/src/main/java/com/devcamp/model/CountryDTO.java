package com.devcamp.model;

import java.util.List;

public class CountryDTO {

	private Long id;

	private String countryCode;

	private String countryName;

	private List<RegionDTO> regions;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public List<RegionDTO> getRegions() {
		return regions;
	}

	public void setRegions(List<RegionDTO> regions) {
		this.regions = regions;
	}
}
