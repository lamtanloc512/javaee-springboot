package com.devcamp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class RegionDTO {
	private Long id;

	private String regionCode;

	private String regionName;

	private CountryDTO country;

	@SuppressWarnings("unused")
	private String countryName;

	public String getCountryName() {
		return getCountry().getCountryName();
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	@JsonIgnore
	public CountryDTO getCountry() {
		return country;
	}

	public void setCountry(CountryDTO cCountry) {
		this.country = cCountry;
	}

}
