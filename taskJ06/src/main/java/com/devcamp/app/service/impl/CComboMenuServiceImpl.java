package com.devcamp.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.app.model.CComboMenu;
import com.devcamp.app.service.IComboMenuService;

@Service
public class CComboMenuServiceImpl implements IComboMenuService {

	@Override
	public List<CComboMenu> getAllComboMenu() {
		List<CComboMenu> listCombo = new ArrayList<CComboMenu>();
		CComboMenu sizeS = new CComboMenu("S", "20cm", "2", "200g", "2", "150000");
		CComboMenu sizeM = new CComboMenu("M", "25cm", "4", "300g", "4", "200000");
		CComboMenu sizeL = new CComboMenu("L", "30cm", "8", "500g", "8", "250000");

		listCombo.add(sizeS);
		listCombo.add(sizeM);
		listCombo.add(sizeL);

		return listCombo;
	}

}
