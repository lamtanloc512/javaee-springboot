package com.devcamp.app.service;

import java.util.List;

import com.devcamp.app.model.CComboMenu;

public interface IComboMenuService {
	List<CComboMenu> getAllComboMenu();
}
