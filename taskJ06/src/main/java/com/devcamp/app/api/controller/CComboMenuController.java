package com.devcamp.app.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.app.model.CComboMenu;
import com.devcamp.app.service.IComboMenuService;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
@RequestMapping("/devcamp")
public class CComboMenuController {

	@Autowired
	private IComboMenuService ccombomenu;

	@GetMapping("/menu-combo")
	public List<CComboMenu> getAllComboMenu() {
		return ccombomenu.getAllComboMenu();
	}

}
