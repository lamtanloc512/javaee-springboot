package com.devcamp.app.api.controller;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.app.model.CSaleDTO;

@CrossOrigin(origins = "*", maxAge = -1)
@RestController
@RequestMapping("/devcamp")
public class CPizzaCampaignController {

	@GetMapping("/date")
	public Map<String,Object> getDateSale(@RequestParam(value = "username", defaultValue = "Pizza Passioner") String name) {
		LocalDate localtime = LocalDate.now();
		DayOfWeek dayofweek = localtime.getDayOfWeek();
		CSaleDTO sale = new CSaleDTO();

		Map<String,Object> list = new HashMap<String,Object>();
		
		switch (dayofweek) {
		case MONDAY:
			sale.setSlogan("Mua 1 tặng 1");
			break;
		case TUESDAY:
			sale.setSlogan("Mua 2 tặng 1");
			break;
		case WEDNESDAY:
			sale.setSlogan("Mua 3 tặng 1");
			break;
		case THURSDAY:
			sale.setSlogan("Mua 1 tặng 2");
			break;
		case FRIDAY:
			sale.setSlogan("Mua 1 tặng 3");
			break;
		case SATURDAY:
			sale.setSlogan("Mua 1 tặng 4");
			break;
		case SUNDAY:
			sale.setSlogan("Mua 2 tặng 5");
			break;
		}
		list.put("sale",sale);
		list.put("today",dayofweek);
		return list;
	}
}
