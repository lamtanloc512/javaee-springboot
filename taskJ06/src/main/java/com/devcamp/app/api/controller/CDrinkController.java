package com.devcamp.app.api.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.app.model.CDrink;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
@RequestMapping("/devcamp")
public class CDrinkController {
	@GetMapping("/drinks")
	public List<CDrink> getAllDrinks(){
		List<CDrink> drinks = new ArrayList<CDrink>();
		LocalDate today = LocalDate.now(ZoneId.systemDefault());
		
		
		CDrink tratac = new CDrink("TRATAC", "Trà tắc", "10000", null, today , today);
		CDrink coca = new CDrink("COCA", "Cocacola", "15000", null, today , today);
		CDrink pepsi = new CDrink("PEPSI", "Pepsi", "15000", null, today , today);
		CDrink lavie = new CDrink("LAVIE", "Lavie", "5000", null, today , today);
		CDrink trasua = new CDrink("TRASUA", "Trà sữa", "40000", null, today , today);
		CDrink fanta = new CDrink("FANTA", "Fanta", "15000", null, today , today);

		drinks.add(tratac);
		drinks.add(coca);
		drinks.add(pepsi);
		drinks.add(lavie);
		drinks.add(trasua);
		drinks.add(fanta);
		
		return drinks;
	}
}
