package com.devcamp.app.model;

public class CSaleDTO {

	private String slogan;

	public CSaleDTO() {

	}

	public CSaleDTO(String slogan) {
		this.slogan = slogan;

	}

	public String getSlogan() {
		return slogan;
	}

	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

}
