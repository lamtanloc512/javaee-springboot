package com.devcamp.app.model;

public class CComboMenu {
	private String kichCo;
	private String duongKinh;
	private String suonnuong;
	private String salad;
	private String nuocNgot;
	private String gia;

	/**
	 * 
	 */
	public CComboMenu() {

	}

	/**
	 * @param kichCo
	 * @param duongKinh
	 * @param suonnuong
	 * @param salad
	 * @param nuocNgot
	 * @param gia
	 */
	public CComboMenu(String kichCo, String duongKinh, String suonnuong, String salad, String nuocNgot, String gia) {
		super();
		this.kichCo = kichCo;
		this.duongKinh = duongKinh;
		this.suonnuong = suonnuong;
		this.salad = salad;
		this.nuocNgot = nuocNgot;
		this.gia = gia;
	}

	// set get

	/**
	 * @return the kichCo
	 */
	public String getKichCo() {
		return kichCo;
	}

	/**
	 * @param kichCo the kichCo to set
	 */
	public void setKichCo(String kichCo) {
		this.kichCo = kichCo;
	}

	/**
	 * @return the duongKinh
	 */
	public String getDuongKinh() {
		return duongKinh;
	}

	/**
	 * @param duongKinh the duongKinh to set
	 */
	public void setDuongKinh(String duongKinh) {
		this.duongKinh = duongKinh;
	}

	/**
	 * @return the suonnuong
	 */
	public String getSuonnuong() {
		return suonnuong;
	}

	/**
	 * @param suonnuong the suonnuong to set
	 */
	public void setSuonnuong(String suonnuong) {
		this.suonnuong = suonnuong;
	}

	/**
	 * @return the nuocNgot
	 */
	public String getNuocNgot() {
		return nuocNgot;
	}

	/**
	 * @param nuocNgot the nuocNgot to set
	 */
	public void setNuocNgot(String nuocNgot) {
		this.nuocNgot = nuocNgot;
	}

	/**
	 * @return the salad
	 */
	public String getSalad() {
		return salad;
	}

	/**
	 * @param salad the salad to set
	 */
	public void setSalad(String salad) {
		this.salad = salad;
	}

	/**
	 * @return the gia
	 */
	public String getGia() {
		return gia;
	}

	/**
	 * @param gia the gia to set
	 */
	public void setGia(String gia) {
		this.gia = gia;
	}

}
