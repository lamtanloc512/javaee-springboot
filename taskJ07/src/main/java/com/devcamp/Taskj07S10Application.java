package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Taskj07S10Application {

	public static void main(String[] args) {
		SpringApplication.run(Taskj07S10Application.class, args);
	}

}
