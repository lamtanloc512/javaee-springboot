package com.devcamp.s50.relationship;

public abstract class CPerson {
	private int age;
	private String gender;
	private String name;
	private CAddress address;

	public CPerson() {

	}

	public CPerson(int age, String gender, String name, CAddress address) {
		super();
		this.age = age;
		this.gender = gender;
		this.name = name;
		this.address = address;
	}

	void eat() {

	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CAddress getAddress() {
		return address;
	}

	public void setAddress(CAddress address) {
		this.address = address;
	}

}
