package com.devcamp.s50.relationship;

public class CWorker extends CPerson {
	private int salary;

	public CWorker() {
	}

	public CWorker(int age, String gender, String name, CAddress address, int salary) {
		super(age, gender, name, address);
		this.salary = salary;
	}

	public void working() {

	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

}
