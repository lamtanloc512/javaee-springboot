package com.devcamp.s50.relationship;

public class CProfessor extends CPerson {
	private int salary;

	public CProfessor() {
	}

	public CProfessor(int age, String gender, String name, CAddress address,int salary) {
		super(age, gender, name, address);
		this.salary = salary;
	}

	public CProfessor(int salary) {
		super();
		this.salary = salary;
	}

	public void teaching() {

	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

}
