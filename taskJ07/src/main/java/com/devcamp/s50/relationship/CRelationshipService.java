package com.devcamp.s50.relationship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class CRelationshipService {

	public Map<String, ArrayList<CPerson>> getAllPersons() {
		ArrayList<CPerson> listStudent = new ArrayList<CPerson>();
		ArrayList<CPerson> listProfessor = new ArrayList<CPerson>();

		Map<String, ArrayList<CPerson>> listofperson = new HashMap<String, ArrayList<CPerson>>();

		CAddress addressStudent1 = new CAddress("119 Nguyễn Tất Thành", "Đồng Nai", "Việt Nam", 8100);
		CAddress addressStudent2 = new CAddress("120 Nguyễn Tất Thành", "Đồng Nai", "Việt Nam", 8100);
		CAddress addressProfessor1 = new CAddress("Số 1 Lê Duẩn", "Tp Hồ Chí Minh", "Việt Nam", 2875);
		CAddress addressProfessor2 = new CAddress("Số 2 Lê Duẩn", "Tp Hồ Chí Minh", "Việt Nam", 2875);

		CPerson student1 = new CStudent(28, "male", "Lâm Tấn Lộc", addressStudent1, 25, 9.5f);
		CPerson student2 = new CStudent(28, "female", "Lâm Tấn Phát", addressStudent2, 25, 8.5f);
		CPerson professor1 = new CProfessor(57, "male", "Lê Thẩm Dương", addressProfessor1, 100);
		CPerson professor2 = new CProfessor(57, "female", "Shark Linh", addressProfessor2, 100);

		listStudent.add(student1);
		listStudent.add(student2);
		listProfessor.add(professor1);
		listProfessor.add(professor2);

		listofperson.put("students", listStudent);
		listofperson.put("professors", listProfessor);

		return listofperson;
	}

}
