package com.devcamp.s50.relationship;

public class CSubject {
	private String subTitle;
	private int subId;
	private CProfessor teacher;

	public CSubject() {
	}

	public CSubject(String subTitle, int subId, CProfessor teacher) {
		super();
		this.subTitle = subTitle;
		this.subId = subId;
		this.teacher = teacher;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public int getSubId() {
		return subId;
	}

	public void setSubId(int subId) {
		this.subId = subId;
	}

	public CProfessor getTeacher() {
		return teacher;
	}

	public void setTeacher(CProfessor teacher) {
		this.teacher = teacher;
	}

}
