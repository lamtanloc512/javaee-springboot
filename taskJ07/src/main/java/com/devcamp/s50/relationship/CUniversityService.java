package com.devcamp.s50.relationship;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CUniversityService {
	@Autowired
	CRelationshipService listofstudents;
	
	public CUniversity getUniversity() {
		CUniversity university = new CUniversity(123, "IRONHACK VIỆT NAM", listofstudents.getAllPersons());
		return university;
	}
}
