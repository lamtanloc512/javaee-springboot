package com.devcamp.s50.relationship;

public class CStudent extends CPerson {
	private int studentId;
	private float averageMark;

	public CStudent() {
	}

	public CStudent(int age, String gender, String name, CAddress address, int studentId, float averageMark) {
		super(age, gender, name, address);
		this.studentId = studentId;
		this.averageMark = averageMark;
	}

	public CStudent(int studentId, float averageMark) {
		super();
		this.studentId = studentId;
		this.averageMark = averageMark;
	}
	
	public void doHomeWork() {
		
	}
	
	@Override
	void eat() {
		super.eat();
	}
	// set get

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public float getAverageMark() {
		return averageMark;
	}

	public void setAverageMark(float averageMark) {
		this.averageMark = averageMark;
	}

}
