package com.devcamp.s50.relationship;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
@RequestMapping("/relationship")
public class CRelationshipController {
	
	@Autowired
	CRelationshipService listOfPersons;
	
	@GetMapping("/all")
	public Map<String, ArrayList<CPerson>> getAllPerson() {
		return listOfPersons.getAllPersons();
	}
	
	@Autowired
	CUniversityService universityInfo;
	
	@GetMapping("/university")
	public CUniversity getCUniversity() {
		return universityInfo.getUniversity();
	}
	
}
