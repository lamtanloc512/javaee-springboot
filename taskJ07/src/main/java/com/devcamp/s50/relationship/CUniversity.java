package com.devcamp.s50.relationship;

import java.util.ArrayList;
import java.util.Map;

public class CUniversity {
	private int code;
	private String name;
	private Map<String, ArrayList<CPerson>> studentlist;

	public CUniversity() {
	}

	public CUniversity(int code, String name, Map<String, ArrayList<CPerson>> studentlist) {
		super();
		this.code = code;
		this.name = name;
		this.studentlist = studentlist;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, ArrayList<CPerson>> getStudentlist() {
		return studentlist;
	}

	public void setStudentlist(Map<String, ArrayList<CPerson>> studentlist) {
		this.studentlist = studentlist;
	}

}
