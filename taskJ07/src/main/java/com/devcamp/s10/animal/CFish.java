package com.devcamp.s10.animal;

import org.springframework.stereotype.Component;

@Component
public class CFish extends CAnimals {

	public CFish() {

	}

	@Override
	void isMammal() {
		super.isMammal();
	}

	@Override
	void mate() {
		super.mate();
	}

	@Override
	void swim() {
		super.swim();
	}

	@Override
	public int getAge() {
		return super.getAge();
	}

	@Override
	public void setAge(int age) {
		super.setAge(age);
	}

	@Override
	public String getGender() {
		return super.getGender();
	}

	@Override
	public void setGender(String gender) {
		super.setGender(gender);
	}

	@Override
	public int getSize() {
		return super.getSize();
	}

	@Override
	public void setSize(int size) {
		super.setSize(size);
	}

	@Override
	public boolean isCanEat() {
		return super.isCanEat();
	}

	@Override
	public void setCanEat(boolean canEat) {
		super.setCanEat(canEat);
	}

}
