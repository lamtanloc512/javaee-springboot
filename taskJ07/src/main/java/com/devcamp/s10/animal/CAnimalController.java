package com.devcamp.s10.animal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CAnimalController {
	
	@Autowired
	CAnimalService cAnimalService;
	
	@GetMapping("/ducks")
	public CDuckDTO duck() {
		return cAnimalService.getDuck();
	}
	@GetMapping("/fishes")
	public CFishDTO fish() {
		return cAnimalService.getFish();
	}
	@GetMapping("/zebras")
	public CZebraDTO zebra() {
		return cAnimalService.getZebra();
	}
	
}
