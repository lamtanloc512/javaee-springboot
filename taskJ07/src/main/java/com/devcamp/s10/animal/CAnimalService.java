package com.devcamp.s10.animal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CAnimalService {
	@Autowired
	CAnimalDao cAnimalDao;

	public CDuckDTO getDuck() {
		return cAnimalDao.getDuckDto();
	}

	public CFishDTO getFish() {
		return cAnimalDao.getFishDto();
	}

	public CZebraDTO getZebra() {
		return cAnimalDao.getZebraDto();
	}
}
