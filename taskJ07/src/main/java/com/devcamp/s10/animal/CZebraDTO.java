package com.devcamp.s10.animal;

public class CZebraDTO {

	private int age;
	private String gender;
	private boolean is_wild;

	public CZebraDTO() {
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isIs_wild() {
		return is_wild;
	}

	public void setIs_wild(boolean is_wild) {
		this.is_wild = is_wild;
	}

}
