package com.devcamp.s10.animal;

import org.springframework.stereotype.Component;

@Component
public class CAnimalDao {

	public CDuckDTO getDuckDto() {
		CDuck duck = new CDuck();
		duck.setAge(4);
		duck.setGender("male");
		duck.setBeakColor("black");

		CDuckDTO duckDto = new CDuckDTO();
		duckDto.setAge(duck.getAge());
		duckDto.setGender(duck.getGender());
		duckDto.setBeakColor(duck.getBeakColor());

		return duckDto;
	}

	public CFishDTO getFishDto() {
		CFish fish = new CFish();
		fish.setAge(2);
		fish.setGender("female");
		fish.setSize(2);
		fish.setCanEat(true);

		CFishDTO fishDto = new CFishDTO();
		fishDto.setAge(fish.getAge());
		fishDto.setGender(fish.getGender());
		fishDto.setSize(fish.getSize());
		fishDto.setCanEat(fish.isCanEat());

		return fishDto;
	}

	public CZebraDTO getZebraDto() {
		CZebra zebra = new CZebra();
		zebra.setAge(5);
		zebra.setGender("female");
		zebra.setIs_wild(true);

		CZebraDTO zebraDto = new CZebraDTO();
		zebraDto.setAge(zebra.getAge());
		zebraDto.setGender(zebra.getGender());
		zebraDto.setIs_wild(zebra.isIs_wild());

		return zebraDto;
	}

}
