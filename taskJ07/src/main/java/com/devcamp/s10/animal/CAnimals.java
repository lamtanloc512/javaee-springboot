package com.devcamp.s10.animal;

abstract class CAnimals {

	private int age;
	private String gender;
	private boolean is_wild;
	private String beakColor;
	private int size;
	private boolean canEat;

	public CAnimals() {

	}

	void isMammal() {
	}

	void mate() {
	}

	void quack() {
	}

	void swim() {
	}

	void run() {
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isIs_wild() {
		return is_wild;
	}

	public void setIs_wild(boolean is_wild) {
		this.is_wild = is_wild;
	}

	public String getBeakColor() {
		return beakColor;
	}

	public void setBeakColor(String beakColor) {
		this.beakColor = beakColor;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isCanEat() {
		return canEat;
	}

	public void setCanEat(boolean canEat) {
		this.canEat = canEat;
	}
	
	
}
