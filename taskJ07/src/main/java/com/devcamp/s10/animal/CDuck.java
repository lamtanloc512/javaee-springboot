package com.devcamp.s10.animal;

public class CDuck extends CAnimals {

	public CDuck() {

	}

	@Override
	void isMammal() {
		super.isMammal();
	}

	@Override
	void mate() {
		super.mate();
	}

	@Override
	void quack() {
		super.quack();
	}

	@Override
	void swim() {
		super.swim();
	}

}
