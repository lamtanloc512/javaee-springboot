package com.devcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "rl_payment")
public class CPayment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "total_price")
	private long totalPrice;

	@Column(name = "status")
	private String status;

	@Column(name = "address")
	private String address;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false , mappedBy = "payment")
	@JsonBackReference
	private COrder order;

}
