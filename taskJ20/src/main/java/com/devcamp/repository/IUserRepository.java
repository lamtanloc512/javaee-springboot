package com.devcamp.repository;

import com.devcamp.model.CUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<CUser, Long> {

}
