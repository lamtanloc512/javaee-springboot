package com.devcamp.taskJ13.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.taskJ13.model.CVoucher;
import com.devcamp.taskJ13.respository.IVoucherRespository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
@RequestMapping("/devcamp")
public class CVoucherController {

    @Autowired
    IVoucherRespository voucheRespository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVoucher() {
	try {
	    List<CVoucher> vouchers = new ArrayList<CVoucher>();
	    voucheRespository.findAll().forEach(vouchers::add);
	    return new ResponseEntity<>(vouchers, HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

    }

}
