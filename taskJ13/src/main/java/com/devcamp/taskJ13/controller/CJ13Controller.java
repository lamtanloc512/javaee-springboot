package com.devcamp.taskJ13.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.taskJ13.model.CComboMenu;
import com.devcamp.taskJ13.model.CCustomer;
import com.devcamp.taskJ13.model.COrder;
import com.devcamp.taskJ13.model.CProduct;
import com.devcamp.taskJ13.model.CVoucher;
import com.devcamp.taskJ13.respository.IComboMenuRespository;
import com.devcamp.taskJ13.respository.ICustomerRespository;
import com.devcamp.taskJ13.respository.IOrderRespository;
import com.devcamp.taskJ13.respository.IProductRespository;
import com.devcamp.taskJ13.respository.IVoucherRespository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CJ13Controller {
	@Autowired
	ICustomerRespository customerRespository;

	@Autowired
	IOrderRespository orderRespository;

	@Autowired
	IProductRespository productRespository;

	@Autowired
	IVoucherRespository voucherRespository;

	@PersistenceContext
	private EntityManager entityManager;

	@GetMapping("/customers")
	public List<CCustomer> getOneCustomer() {
		return customerRespository.findAll();
	}


	@GetMapping("/orders/id")
	public ResponseEntity<List<CCustomer>> getAllOrderById(@RequestParam(name = "userId") Long id) {
		List<CCustomer> listOrder = new ArrayList<CCustomer>();
		try {
			List<CCustomer> customers = customerRespository.findAll();
			for (CCustomer oneCustomer : customers) {
				if (oneCustomer.getId() == id) {
					listOrder.add(oneCustomer);
				}
			}
			return new ResponseEntity<>(listOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/orders")
	public ResponseEntity<List<COrder>> getAllOrders() {
		try {
			return new ResponseEntity<>(orderRespository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "products")
	public ResponseEntity<List<CProduct>> getProductsByOrderId(@RequestParam(name = "orderId") Long orderId) {
		List<CProduct> productList = new ArrayList<CProduct>();
		try {
			for (CProduct productElement : productRespository.findAll()) {
				if (productElement.getOrderKey().getOrderId() == orderId) {
					productList.add(productElement);
				}
			}
			return new ResponseEntity<List<CProduct>>(productList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "products/orderkey")
	public ResponseEntity<List<CProduct>> getProductsByOrderKey(
			@RequestParam(name = "orderKey", defaultValue = "0") COrder orderId) {
		try {
			if (orderId.equals(0)) {
				System.out.println("dung roi");
				return new ResponseEntity<List<CProduct>>(productRespository.findAll(), HttpStatus.OK);
			} else {
				return new ResponseEntity<List<CProduct>>(productRespository.findByOrderKey(orderId), HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/vouchers")
	public ResponseEntity<List<CVoucher>> getAllVouchers() {
		try {
			return new ResponseEntity<>(voucherRespository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@Autowired
	IComboMenuRespository comboMenuRespository;

	@GetMapping(path = "/combos")
	public ResponseEntity<List<CComboMenu>> getAllComboMenus() {
		try {
			return new ResponseEntity<>(comboMenuRespository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
