package com.devcamp.taskJ13.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.taskJ13.model.CVoucher;

public interface IVoucherRespository extends JpaRepository<CVoucher, Long> {
    
}
