package com.devcamp.taskJ13.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.taskJ13.model.COrder;
import com.devcamp.taskJ13.model.CProduct;

public interface IProductRespository extends JpaRepository<CProduct, Long> {
    List<CProduct> findByOrderKey(COrder orderkey);
}
