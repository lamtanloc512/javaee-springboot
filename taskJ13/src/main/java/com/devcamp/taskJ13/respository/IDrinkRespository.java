package com.devcamp.taskJ13.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.taskJ13.model.CDrink;

@Repository
public interface IDrinkRespository extends JpaRepository<CDrink, Long> {

}
