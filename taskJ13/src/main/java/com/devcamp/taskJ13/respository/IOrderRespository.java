package com.devcamp.taskJ13.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.taskJ13.model.COrder;

public interface IOrderRespository extends JpaRepository<COrder, Long> {
    
}
