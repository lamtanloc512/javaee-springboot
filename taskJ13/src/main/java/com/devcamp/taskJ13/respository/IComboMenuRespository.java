package com.devcamp.taskJ13.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.taskJ13.model.CComboMenu;

public interface IComboMenuRespository extends JpaRepository<CComboMenu, Long> {

}
