package com.devcamp.taskJ13.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "drinks")
public class CDrink {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long drink_id;

    @Column(name = "ma_nuoc_uong")
    private String maNuocUong;

    @Column(name = "ten_nuoc_uong")
    private String tenNuocUong;

    @Column(name = "don_gia")
    private String donGia;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    /**
     * 
     */
    public CDrink() {
	super();
    }

    /**
     * @param maNuocUong
     * @param tenNuocUong
     * @param donGia
     * @param ghiChu
     * @param ngayTao
     * @param ngayCapNhat
     */
    public CDrink(String maNuocUong, String tenNuocUong, String donGia, String ghiChu, LocalDate ngayTao,
	    LocalDate ngayCapNhat) {
	super();
	this.maNuocUong = maNuocUong;
	this.tenNuocUong = tenNuocUong;
	this.donGia = donGia;
	this.ghiChu = ghiChu;
	this.ngayTao = ngayTao;
	this.ngayCapNhat = ngayCapNhat;
    }

    /**
     * @return the maNuocUong
     */
    public String getMaNuocUong() {
	return maNuocUong;
    }

    /**
     * @param maNuocUong the maNuocUong to set
     */
    public void setMaNuocUong(String maNuocUong) {
	this.maNuocUong = maNuocUong;
    }

    /**
     * @return the tenNuocUong
     */
    public String getTenNuocUong() {
	return tenNuocUong;
    }

    /**
     * @param tenNuocUong the tenNuocUong to set
     */
    public void setTenNuocUong(String tenNuocUong) {
	this.tenNuocUong = tenNuocUong;
    }

    /**
     * @return the donGia
     */
    public String getDonGia() {
	return donGia;
    }

    /**
     * @param donGia the donGia to set
     */
    public void setDonGia(String donGia) {
	this.donGia = donGia;
    }

    /**
     * @return the ghiChu
     */
    public String getGhiChu() {
	return ghiChu;
    }

    /**
     * @param ghiChu the ghiChu to set
     */
    public void setGhiChu(String ghiChu) {
	this.ghiChu = ghiChu;
    }

    /**
     * @return the ngayTao
     */
    public LocalDate getNgayTao() {
	return ngayTao;
    }

    /**
     * @param ngayTao the ngayTao to set
     */
    public void setNgayTao(LocalDate ngayTao) {
	this.ngayTao = ngayTao;
    }

    /**
     * @return the ngayCapNhat
     */
    public LocalDate getNgayCapNhat() {
	return ngayCapNhat;
    }

    /**
     * @param ngayCapNhat the ngayCapNhat to set
     */
    public void setNgayCapNhat(LocalDate ngayCapNhat) {
	this.ngayCapNhat = ngayCapNhat;
    }

}
