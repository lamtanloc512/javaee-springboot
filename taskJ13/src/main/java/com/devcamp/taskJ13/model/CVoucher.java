package com.devcamp.taskJ13.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vouchers")
public class CVoucher {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long voucher_id;

    @Column(name = "phan_tram_giam_gia")
    private String phanTramGiamGia;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Column(name = "maVoucher")
    private String maVoucher;

    @Column(name = "ngay_tao")
    private LocalDate ngayTao;

    @Column(name = "ngay_cap_nhat")
    private LocalDate ngayCapNhat;

    public CVoucher() {
	super();
    }

    public CVoucher(long id, String phanTramGiamGia, String ghiChu, String maVoucher, LocalDate ngayTao,
	    LocalDate ngayCapNhat) {
	super();
	this.voucher_id = id;
	this.phanTramGiamGia = phanTramGiamGia;
	this.ghiChu = ghiChu;
	this.maVoucher = maVoucher;
	this.ngayTao = ngayTao;
	this.ngayCapNhat = ngayCapNhat;
    }

    public long getId() {
	return voucher_id;
    }

    public void setId(long id) {
	this.voucher_id = id;
    }

    public String getPhanTramGiamGia() {
	return phanTramGiamGia;
    }

    public void setPhanTramGiamGia(String phanTramGiamGia) {
	this.phanTramGiamGia = phanTramGiamGia;
    }

    public String getGhiChu() {
	return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
	this.ghiChu = ghiChu;
    }

    public String getMaVoucher() {
	return maVoucher;
    }

    public void setMaVoucher(String maVoucher) {
	this.maVoucher = maVoucher;
    }

    public LocalDate getNgayTao() {
	return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
	this.ngayTao = ngayTao;
    }

    public LocalDate getNgayCapNhat() {
	return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
	this.ngayCapNhat = ngayCapNhat;
    }

}
