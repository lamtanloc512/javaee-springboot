package com.devcamp.taskJ13.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "products")
public class CProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long product_id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_code")
    private int productCode;

    @Column(name = "product_price")
    private int price;

    @ManyToOne
    @JoinColumn(name = "order_id_fk", referencedColumnName = "orderId")
    @JsonBackReference
    private COrder orderKey;

    @Column(name = "date_created")
    private LocalDate ngayTao;

    @Column(name = "date_updated")
    private LocalDate ngayCapNhat;

    public CProduct(long productId, String productName, int productCode, int price, LocalDate ngayTao,
	    LocalDate ngayCapNhat) {
	super();
	this.product_id = productId;
	this.productName = productName;
	this.productCode = productCode;
	this.price = price;
	this.ngayTao = ngayTao;
	this.ngayCapNhat = ngayCapNhat;
    }

    public CProduct() {
	super();
    }

    public long getProductId() {
	return product_id;
    }

    public void setProductId(long productId) {
	this.product_id = productId;
    }

    public String getProductName() {
	return productName;
    }

    public void setProductName(String productName) {
	this.productName = productName;
    }

    public int getProductCode() {
	return productCode;
    }

    public void setProductCode(int productCode) {
	this.productCode = productCode;
    }

    public int getPrice() {
	return price;
    }

    public void setPrice(int price) {
	this.price = price;
    }

    public LocalDate getNgayTao() {
	return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
	this.ngayTao = ngayTao;
    }

    public LocalDate getNgayCapNhat() {
	return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
	this.ngayCapNhat = ngayCapNhat;
    }

    public long getProduct_id() {
	return product_id;
    }

    public void setProduct_id(long product_id) {
	this.product_id = product_id;
    }

    public COrder getOrderKey() {
	return orderKey;
    }

    public void setOrderKey(COrder orderKey) {
	this.orderKey = orderKey;
    }

}
