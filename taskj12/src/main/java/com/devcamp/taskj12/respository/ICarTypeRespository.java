package com.devcamp.taskj12.respository;

import com.devcamp.taskj12.model.Car;
import com.devcamp.taskj12.model.CarType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICarTypeRespository extends JpaRepository<CarType, Integer> {
}
