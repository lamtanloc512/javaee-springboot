package com.devcamp.taskj12.respository;

import com.devcamp.taskj12.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICarRespository extends JpaRepository<Car, Integer> {
    Car findByCarCode(String car);
}
