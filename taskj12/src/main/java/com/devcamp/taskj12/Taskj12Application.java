package com.devcamp.taskj12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Taskj12Application {

    public static void main(String[] args) {
        SpringApplication.run(Taskj12Application.class, args);
    }

}
