package com.devcamp.taskj12.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int car_id;

    @Column(name = "car_code")
    private String carCode;

    @Column(name = "car_name")
    private String carName;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CarType> types;

    public Car() {
    }

    public Car(int car_id, String carCode, String carName, Set<CarType> types) {
        this.car_id = car_id;
        this.carCode = carCode;
        this.carName = carName;
        this.types = types;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Set<CarType> getTypes() {
        return types;
    }

    public void setTypes(Set<CarType> types) {
        this.types = types;
    }
}
