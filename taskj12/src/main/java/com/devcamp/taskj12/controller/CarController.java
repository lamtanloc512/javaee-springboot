package com.devcamp.taskj12.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.taskj12.model.Car;
import com.devcamp.taskj12.respository.ICarRespository;
import com.devcamp.taskj12.respository.ICarTypeRespository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CarController {
    @Autowired
    ICarRespository carRespository;

    @Autowired
    ICarTypeRespository carTypeRespository;

    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getCarList() {
	try {
	    List<Car> list = new ArrayList<Car>(carRespository.findAll());
	    return new ResponseEntity<>(list, HttpStatus.OK);
	} catch (Exception e) {
	    System.out.println(e);
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

    }

    @GetMapping("/car-types")
    public ResponseEntity<Car> getCarTypes(@RequestParam(name = "carcode") String carcode) {
	try {
	    Car car = carRespository.findByCarCode(carcode);
	    System.out.println(car);
	    return new ResponseEntity<>(car, HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }
}
