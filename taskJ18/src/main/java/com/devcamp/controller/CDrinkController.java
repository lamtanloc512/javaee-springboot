package com.devcamp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CDrink;
import com.devcamp.repository.IDrinkRepository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
@RequestMapping(path = "/drinks")
public class CDrinkController {

    @Autowired
    IDrinkRepository drinkRespository;

    @GetMapping("/{id}")
    public ResponseEntity<Object> getDrinkById(@PathVariable(name = "id") Long id) {
	Optional<CDrink> drinkFounded = drinkRespository.findById(id);

	if (drinkFounded.isPresent()) {
	    return new ResponseEntity<>(drinkFounded.get(), HttpStatus.OK);
	} else {
	    return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
	try {
	    return new ResponseEntity<>(drinkRespository.findAll(), HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

	}
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Object> createDrink(@RequestBody CDrink newDrink) {
	try {
	    CDrink _drink = new CDrink();
	    _drink.setDonGia(newDrink.getDonGia());
	    _drink.setGhiChu(newDrink.getGhiChu());
	    _drink.setMaNuocUong(newDrink.getMaNuocUong());
	    _drink.setTenNuocUong(newDrink.getTenNuocUong());
	    _drink.setNgayTao(new Date());

	    return new ResponseEntity<>(drinkRespository.save(_drink), HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<Object> updateDrink(@RequestBody CDrink updateDrink, @PathVariable(name = "id") Long id) {
	Optional<CDrink> _drinkData = drinkRespository.findById(id);
	if (_drinkData.isPresent()) {
	    CDrink _drink = _drinkData.get();
	    _drink.setDonGia(updateDrink.getDonGia());
	    _drink.setGhiChu(updateDrink.getGhiChu());
	    _drink.setMaNuocUong(updateDrink.getMaNuocUong());
	    _drink.setTenNuocUong(updateDrink.getTenNuocUong());
	    _drink.setNgayCapNhat(new Date());
	    return new ResponseEntity<>(drinkRespository.save(_drink), HttpStatus.OK);

	} else {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> deleteDrinkById(@PathVariable(name = "id") Long id) {
	try {
	    drinkRespository.deleteById(id);
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

}
