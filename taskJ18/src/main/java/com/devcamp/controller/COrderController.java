package com.devcamp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.COrder;
import com.devcamp.repository.ICorderRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping(path = "/v1/order")
public class COrderController {

    @Autowired
    ICorderRepository orderRepository;

    @CrossOrigin
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable Long id) {
	try {
	    orderRepository.deleteById(id);
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	} catch (Exception e) {
	    return ResponseEntity.unprocessableEntity()
		    .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
	}
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "id") Long id, @RequestBody COrder orderUpdate) {
	Optional<COrder> _orderData = orderRepository.findById(id);
	if (_orderData.isPresent()) {
	    COrder _order = _orderData.get();
	    _order.setUpdated(new Date());
	    _order.setPizzaSize(orderUpdate.getPizzaSize());
	    _order.setPizzaType(orderUpdate.getPizzaType());
	    _order.setPrice(orderUpdate.getPrice());
	    _order.setPaid(orderUpdate.getPaid());
	    _order.setVoucherCode(orderUpdate.getVoucherCode());
	    try {
		return ResponseEntity.ok(orderRepository.save(_order));
	    } catch (Exception e) {
		return ResponseEntity.unprocessableEntity()
			.body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
	    }
	} else {
	    return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

    }

    @PostMapping(path = "/create")
    public ResponseEntity<Object> createOrder(@RequestBody COrder newOrder) {
	COrder _order = new COrder();
	try {
	    _order.setCreated(new Date());
	    _order.setUpdated(null);
	    _order.setOrderCode(newOrder.getOrderCode());
	    _order.setPizzaSize(newOrder.getPizzaSize());
	    _order.setPizzaType(newOrder.getPizzaType());
	    _order.setPrice(newOrder.getPrice());
	    _order.setPaid(newOrder.getPaid());
	    orderRepository.save(_order);
	    return new ResponseEntity<Object>(_order, HttpStatus.OK);
	} catch (Exception e) {
	    return ResponseEntity.unprocessableEntity()
		    .body("Can not execute operation about this Entity " + e.getCause().getMessage());
	}

    }

    @GetMapping(path = "/search/userId={id}")
    public ResponseEntity<Object> getOrderByUserId(@PathVariable(name = "id") Long id) {
	List<COrder> _order = orderRepository.findBycUserId(id);
	if (_order.size() > 0) {
	    return new ResponseEntity<Object>(_order, HttpStatus.OK);
	} else {
	    return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
	}

    }

    @GetMapping(path = "/all")
    public ResponseEntity<Object> getAllOrders() {
	List<COrder> orderList = new ArrayList<COrder>();
	try {
	    orderRepository.findAll().forEach(orderElement -> {
		orderList.add(orderElement);
	    });
	    return new ResponseEntity<Object>(orderList, HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
	}

    }

    @GetMapping(path = "/detail/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable(name = "id") Long id) {
	Optional<COrder> _order = orderRepository.findById(id);
	if (_order.isPresent()) {
	    return new ResponseEntity<Object>(_order, HttpStatus.OK);
	} else {
	    return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
	}
    }

}
