package com.devcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CCountry;
import com.devcamp.repository.ICountryRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/v1/country")
public class CCountryController {

    @Autowired
    private ICountryRepository countryRepository;

    @GetMapping("/get")
    public ResponseEntity<List<CCountry>> getAllCountry() {
        List<CCountry> allCountry = countryRepository.findAll();
        try {
            return new ResponseEntity<>(allCountry, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/create")
    public ResponseEntity<CCountry> createCountry(@RequestBody CCountry newCountry) {

        CCountry _country = new CCountry();
        _country.setCountryCode(newCountry.getCountryCode());
        _country.setCountryName(newCountry.getCountryName());
        _country.setRegions(newCountry.getRegions());
        try {
            countryRepository.save(_country);
            return new ResponseEntity<>(newCountry, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/update")
    public ResponseEntity<Object> updateCountry(@RequestParam(name = "id", required = true) Long id,
                                                @RequestBody CCountry cCountry) {
        Optional<CCountry> country_found = countryRepository.findById(id);
        if (country_found.isPresent()) {
            CCountry updateCountry = country_found.get();
            updateCountry.setCountryName(cCountry.getCountryName());
            updateCountry.setCountryCode(cCountry.getCountryCode());
            updateCountry.setRegions(cCountry.getRegions());
            CCountry savedCountry = countryRepository.save(updateCountry);
            return new ResponseEntity<>(savedCountry, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/delete")
    public ResponseEntity<Object> deleteCountry(@RequestParam(name = "id") Long id) {
        Optional<CCountry> _country = countryRepository.findById(id);
        if (_country.isPresent()) {
            countryRepository.deleteById(id);
            return new ResponseEntity<>("Deleted", HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
