package com.devcamp.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CComboMenu;
import com.devcamp.repository.IComboMenuRespository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
@RequestMapping(path = "/combos")
public class CComboMenuController {

    @Autowired
    IComboMenuRespository comboMenuRespository;

    @GetMapping("/{id}")
    public ResponseEntity<Object> getComboById(@PathVariable(name = "id") Long id) {
	Optional<CComboMenu> comboFounded = comboMenuRespository.findById(id);

	if (comboFounded.isPresent()) {
	    return new ResponseEntity<>(comboFounded.get(), HttpStatus.OK);
	} else {
	    return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

    }

    @GetMapping(path = "/all")
    public ResponseEntity<Object> getAllComboMenu() {
	try {
	    return new ResponseEntity<>(comboMenuRespository.findAll(), HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

	}
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Object> createDrink(@RequestBody CComboMenu newComboMenu) {
	try {
	    CComboMenu _comboMenu = new CComboMenu();
	    _comboMenu.setGia(newComboMenu.getGia());
	    _comboMenu.setDuongKinh(newComboMenu.getDuongKinh());
	    _comboMenu.setKichCo(newComboMenu.getKichCo());
	    _comboMenu.setNuocNgot(newComboMenu.getNuocNgot());
	    _comboMenu.setSalad(newComboMenu.getSalad());
	    _comboMenu.setSuonnuong(newComboMenu.getSalad());
	    return new ResponseEntity<>(comboMenuRespository.save(_comboMenu), HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<Object> updateDrink(@RequestBody CComboMenu updateComboMenu,
	    @PathVariable(name = "id") Long id) {
	Optional<CComboMenu> _comboMenuData = comboMenuRespository.findById(id);
	if (_comboMenuData.isPresent()) {
	    CComboMenu _comboMenu = new CComboMenu();
	    _comboMenu.setGia(updateComboMenu.getGia());
	    _comboMenu.setDuongKinh(updateComboMenu.getDuongKinh());
	    _comboMenu.setKichCo(updateComboMenu.getKichCo());
	    _comboMenu.setNuocNgot(updateComboMenu.getNuocNgot());
	    _comboMenu.setSalad(updateComboMenu.getSalad());
	    _comboMenu.setSuonnuong(updateComboMenu.getSalad());
	    return new ResponseEntity<>(comboMenuRespository.save(_comboMenu), HttpStatus.OK);

	} else {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> deleteDrinkById(@PathVariable(name = "id") Long id) {
	try {
	    comboMenuRespository.deleteById(id);
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

}
