package com.devcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CCountry;
import com.devcamp.model.CRegion;
import com.devcamp.repository.ICountryRepository;
import com.devcamp.repository.IRegionRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/v1/regions")
public class CRegionController {
	@Autowired
	private IRegionRepository regionRepository;

	@Autowired
	private ICountryRepository countryRepository;

	@CrossOrigin
	@PostMapping("/create/{id}")
	public ResponseEntity<Object> createRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				CRegion newRegion = new CRegion();
				newRegion.setRegionName(cRegion.getRegionName());
				newRegion.setRegionCode(cRegion.getRegionCode());
				newRegion.setCountry(cRegion.getCountry());

				CCountry _country = countryData.get();
				newRegion.setCountry(_country);
				newRegion.setCountryName(_country.getCountryName());

				CRegion savedRegion = regionRepository.save(newRegion);
				return new ResponseEntity<>(savedRegion, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		Optional<CRegion> regionData = regionRepository.findById(id);
		if (regionData.isPresent()) {
			CRegion newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			CRegion savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/details/{id}")
	public ResponseEntity<Object> getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return new ResponseEntity<Object>(regionRepository.findById(id).get(), HttpStatus.OK);
		else
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@GetMapping("/all")
	public ResponseEntity<Object> getAllRegion() {
		List<CRegion> regionsList = regionRepository.findAll();
		if (!regionsList.isEmpty()) {
			return new ResponseEntity<>(regionsList, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions")
	public ResponseEntity<Object> getRegionsByCountryId(@PathVariable(value = "countryId") Long countryId) {
		List<CRegion> countryList = regionRepository.findByCountryId(countryId);

		if (!countryList.isEmpty()) {
			return new ResponseEntity<Object>(countryList, HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions/{id}")
	public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,
			@PathVariable(value = "id") Long regionId) {
		return regionRepository.findByIdAndCountryId(regionId, countryId);
	}

}
