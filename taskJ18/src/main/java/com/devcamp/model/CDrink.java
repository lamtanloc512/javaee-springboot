package com.devcamp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "pizza_drinks")
public class CDrink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "ma_nuoc_uong")
    private String maNuocUong;

    @Column(name = "ten_nuoc_uong")
    private String tenNuocUong;

    @Column(name = "don_gia")
    private String donGia;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @CreatedDate
    @Column(name = "ngay_tao")
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @LastModifiedDate
    @Column(name = "ngay_cap_nhat")
    private Date ngayCapNhat;

    /**
     * 
     */
    public CDrink() {
	super();
    }

    /**
     * @param maNuocUong
     * @param tenNuocUong
     * @param donGia
     * @param ghiChu
     * @param ngayTao
     * @param ngayCapNhat
     */
    public CDrink(String maNuocUong, String tenNuocUong, String donGia, String ghiChu, Date ngayTao, Date ngayCapNhat) {
	super();
	this.maNuocUong = maNuocUong;
	this.tenNuocUong = tenNuocUong;
	this.donGia = donGia;
	this.ghiChu = ghiChu;
	this.ngayTao = ngayTao;
	this.ngayCapNhat = ngayCapNhat;
    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    /**
     * @return the maNuocUong
     */
    public String getMaNuocUong() {
	return maNuocUong;
    }

    /**
     * @param maNuocUong the maNuocUong to set
     */
    public void setMaNuocUong(String maNuocUong) {
	this.maNuocUong = maNuocUong;
    }

    /**
     * @return the tenNuocUong
     */
    public String getTenNuocUong() {
	return tenNuocUong;
    }

    /**
     * @param tenNuocUong the tenNuocUong to set
     */
    public void setTenNuocUong(String tenNuocUong) {
	this.tenNuocUong = tenNuocUong;
    }

    /**
     * @return the donGia
     */
    public String getDonGia() {
	return donGia;
    }

    /**
     * @param donGia the donGia to set
     */
    public void setDonGia(String donGia) {
	this.donGia = donGia;
    }

    /**
     * @return the ghiChu
     */
    public String getGhiChu() {
	return ghiChu;
    }

    /**
     * @param ghiChu the ghiChu to set
     */
    public void setGhiChu(String ghiChu) {
	this.ghiChu = ghiChu;
    }

    /**
     * @return the ngayTao
     */
    public Date getNgayTao() {
	return ngayTao;
    }

    /**
     * @param date the ngayTao to set
     */
    public void setNgayTao(Date date) {
	this.ngayTao = date;
    }

    /**
     * @return the ngayCapNhat
     */
    public Date getNgayCapNhat() {
	return ngayCapNhat;
    }

    /**
     * @param ngayCapNhat the ngayCapNhat to set
     */
    public void setNgayCapNhat(Date ngayCapNhat) {
	this.ngayCapNhat = ngayCapNhat;
    }

}
