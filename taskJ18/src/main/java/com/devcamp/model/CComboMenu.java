package com.devcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pizza_cmenu")
public class CComboMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "kich_co")
    private String kichCo;

    @Column(name = "duong_kinh")
    private String duongKinh;

    @Column(name = "suon_nuong")
    private String suonnuong;

    @Column(name = "salad")
    private String salad;

    @Column(name = "nuoc_ngot")
    private String nuocNgot;

    @Column(name = "gia")
    private Long gia;

    /**
     * 
     */
    public CComboMenu() {

    }

    /**
     * @param kichCo
     * @param duongKinh
     * @param suonnuong
     * @param salad
     * @param nuocNgot
     * @param gia
     */
    public CComboMenu(String kichCo, String duongKinh, String suonnuong, String salad, String nuocNgot, Long gia) {
	super();
	this.kichCo = kichCo;
	this.duongKinh = duongKinh;
	this.suonnuong = suonnuong;
	this.salad = salad;
	this.nuocNgot = nuocNgot;
	this.gia = gia;
    }

    // set get

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    /**
     * @return the kichCo
     */
    public String getKichCo() {
	return kichCo;
    }

    /**
     * @param kichCo the kichCo to set
     */
    public void setKichCo(String kichCo) {
	this.kichCo = kichCo;
    }

    /**
     * @return the duongKinh
     */
    public String getDuongKinh() {
	return duongKinh;
    }

    /**
     * @param duongKinh the duongKinh to set
     */
    public void setDuongKinh(String duongKinh) {
	this.duongKinh = duongKinh;
    }

    /**
     * @return the suonnuong
     */
    public String getSuonnuong() {
	return suonnuong;
    }

    /**
     * @param suonnuong the suonnuong to set
     */
    public void setSuonnuong(String suonnuong) {
	this.suonnuong = suonnuong;
    }

    /**
     * @return the nuocNgot
     */
    public String getNuocNgot() {
	return nuocNgot;
    }

    /**
     * @param nuocNgot the nuocNgot to set
     */
    public void setNuocNgot(String nuocNgot) {
	this.nuocNgot = nuocNgot;
    }

    /**
     * @return the salad
     */
    public String getSalad() {
	return salad;
    }

    /**
     * @param salad the salad to set
     */
    public void setSalad(String salad) {
	this.salad = salad;
    }

    /**
     * @return the gia
     */
    public Long getGia() {
	return gia;
    }

    /**
     * @param gia the gia to set
     */
    public void setGia(Long gia) {
	this.gia = gia;
    }

}
