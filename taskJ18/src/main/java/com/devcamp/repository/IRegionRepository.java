package com.devcamp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CRegion;

public interface IRegionRepository extends JpaRepository<CRegion, Long> {
    List<CRegion> findByCountryId(Long countryId);

    Optional<CRegion> findByIdAndCountryId(Long id, Long instructorId);
}
