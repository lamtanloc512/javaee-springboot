package com.devcamp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CComboMenu;

public interface IComboMenuRespository extends JpaRepository<CComboMenu, Long> {

}
