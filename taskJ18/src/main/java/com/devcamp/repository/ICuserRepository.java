package com.devcamp.repository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.model.CUser;

public interface ICuserRepository extends CrudRepository<CUser, Long>  {
    
}
