package com.devcamp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CCountry;

public interface ICountryRepository extends JpaRepository<CCountry, Long> {
    CCountry findByCountryCode(String countryCode);

    List<CCountry> findAll();
    
}
