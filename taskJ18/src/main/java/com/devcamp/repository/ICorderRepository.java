package com.devcamp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.model.COrder;

public interface ICorderRepository extends CrudRepository<COrder, Long> {
    List<COrder> findBycUserId(Long id);
}
