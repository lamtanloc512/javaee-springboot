package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Taskj12HomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(Taskj12HomeworkApplication.class, args);
	}

}
