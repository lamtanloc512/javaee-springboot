package com.devcamp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.Customer;
import com.devcamp.model.Order;
import com.devcamp.respository.ICustomerRespository;
import com.devcamp.respository.IOrderRespository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CJ12Controller {
    
    @Autowired
    ICustomerRespository customerRespository;
    
    @Autowired
    IOrderRespository orderRespository;
    
    @GetMapping("/customers")
    public List<Customer> getOneCustomer() {
	return customerRespository.findAll();
    }

    @GetMapping("/orders/id")
    public ResponseEntity<List<Customer>> getAllOrderById(@RequestParam(name = "userId") Long id) {
	List<Customer> listOrder = new ArrayList<Customer>();
	try {
	    List<Customer> customers = customerRespository.findAll();
	    for (Customer oneCustomer : customers) {
		if (oneCustomer.getId() == id) {
		    listOrder.add(oneCustomer);
		}
	    }
	    return new ResponseEntity<>(listOrder, HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrders() {
	try {
	    return new ResponseEntity<>(orderRespository.findAll(), HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

}
