package com.devcamp.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.Order;

public interface IOrderRespository extends JpaRepository<Order, Long> {
    
}
