package com.devcamp.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.Customer;

public interface ICustomerRespository extends JpaRepository<Customer, Long>{

}
