package com.devcamp.s40;

import java.util.List;

public class CBook {
    private String name;
    private float price;
    private List<CChuong> chuongList;

    public CBook() {
    }

    public CBook(String name, float price, List<CChuong> chuongList) {
        this.name = name;
        this.price = price;
        this.chuongList = chuongList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<CChuong> getChuongList() {
        return chuongList;
    }

    public void setChuongList(List<CChuong> chuongList) {
        this.chuongList = chuongList;
    }
}
