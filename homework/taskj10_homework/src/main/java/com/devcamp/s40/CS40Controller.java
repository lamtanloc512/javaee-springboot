package com.devcamp.s40;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CS40Controller {

    @GetMapping("/books")
    public List<CBook> getBooks() {
        List<CBook> bookList = new ArrayList<CBook>();

        CBook book1 = new CBook("book1", 7500, new ArrayList<CChuong>() {
            {
                add(new CChuong("chuong 1", new ArrayList<CBai>() {
                    {
                        add(new CBai("bai 1", " content 1"));
                        add(new CBai("bai 2", " content 2"));
                        add(new CBai("bai 3", " content 3"));
                        add(new CBai("bai 4", " content 4"));
                    }
                }));
                add(new CChuong("chuong 2", new ArrayList<CBai>() {
                    {
                        add(new CBai("bai 1", " content 1"));
                        add(new CBai("bai 2", " content 2"));
                        add(new CBai("bai 3", " content 3"));
                        add(new CBai("bai 4", " content 4"));
                    }
                }));
                add(new CChuong("chuong 3", new ArrayList<CBai>() {
                    {
                        add(new CBai("bai 1", " content 1"));
                        add(new CBai("bai 2", " content 2"));
                        add(new CBai("bai 3", " content 3"));
                        add(new CBai("bai 4", " content 4"));
                    }
                }));
            }
        });
        CBook book2 = new CBook("book2", 7500, new ArrayList<CChuong>() {
            {
                add(new CChuong("chuong 1", new ArrayList<CBai>() {
                    {
                        add(new CBai("bai 1", " content 1"));
                        add(new CBai("bai 2", " content 2"));
                        add(new CBai("bai 3", " content 3"));
                        add(new CBai("bai 4", " content 4"));
                    }
                }));
                add(new CChuong("chuong 2", new ArrayList<CBai>() {
                    {
                        add(new CBai("bai 1", " content 1"));
                        add(new CBai("bai 2", " content 2"));
                        add(new CBai("bai 3", " content 3"));
                        add(new CBai("bai 4", " content 4"));
                    }
                }));
                add(new CChuong("chuong 3", new ArrayList<CBai>() {
                    {
                        add(new CBai("bai 1", " content 1"));
                        add(new CBai("bai 2", " content 2"));
                        add(new CBai("bai 3", " content 3"));
                        add(new CBai("bai 4", " content 4"));
                    }
                }));
            }
        });
        CBook book3 = new CBook("book3", 7500, new ArrayList<CChuong>() {
            {
                add(new CChuong("chuong 1", new ArrayList<CBai>() {
                    {
                        add(new CBai("bai 1", " content 1"));
                        add(new CBai("bai 2", " content 2"));
                        add(new CBai("bai 3", " content 3"));
                        add(new CBai("bai 4", " content 4"));
                    }
                }));
            }
        });
        CBook book4 = new CBook("book4", 7500, new ArrayList<CChuong>() {
            {
                add(new CChuong("chuong 1", new ArrayList<CBai>() {
                    {
                        add(new CBai("bai 1", " content 1"));
                        add(new CBai("bai 2", " content 2"));
                        add(new CBai("bai 3", " content 3"));
                        add(new CBai("bai 4", " content 4"));
                    }
                }));
            }
        });

        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        bookList.add(book4);

        return bookList;
    }
}
