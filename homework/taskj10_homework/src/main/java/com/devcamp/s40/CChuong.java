package com.devcamp.s40;

import java.util.List;

public class CChuong {
    private String title;
    private List<CBai> baiList;

    public CChuong() {
    }

    public CChuong(String title, List<CBai> baiList) {
        this.title = title;
        this.baiList = baiList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<CBai> getBaiList() {
        return baiList;
    }

    public void setBaiList(List<CBai> baiList) {
        this.baiList = baiList;
    }
}
