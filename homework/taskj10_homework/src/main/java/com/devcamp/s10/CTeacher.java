package com.devcamp.s10;

import java.time.LocalDateTime;

public class CTeacher extends CPerson {

	public CTeacher() {
		super();
	}

	public CTeacher(int personCode, String personName, String gender, LocalDateTime birthday, String address,
			String phoneNumber) {
		super(personCode, personName, gender, birthday, address, phoneNumber);
	}
	
}
