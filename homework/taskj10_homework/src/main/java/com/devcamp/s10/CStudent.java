package com.devcamp.s10;

import java.time.LocalDateTime;

public class CStudent extends CPerson {
	public CStudent() {
		super();
	}

	public CStudent(int personCode, String personName, String gender, String address, LocalDateTime birthday,
			String phoneNumber) {
		super(personCode, personName, gender, birthday, address, phoneNumber);
	}

}
