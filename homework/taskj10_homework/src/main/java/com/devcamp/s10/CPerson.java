package com.devcamp.s10;

import java.time.LocalDateTime;

public abstract class CPerson {
	private int personCode;
	private String personName;
	private String gender;
	private LocalDateTime birthday;
	private String address;
	private String phoneNumber;

	public CPerson() {
		super();
	}

	public CPerson(int personCode, String personName, String gender, LocalDateTime birthday, String address,
			String phoneNumber) {
		super();
		this.personCode = personCode;
		this.personName = personName;
		this.gender = gender;
		this.birthday = birthday;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}

	public int getPersonCode() {
		return personCode;
	}

	public void setPersonCode(int personCode) {
		this.personCode = personCode;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public LocalDateTime getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDateTime birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
