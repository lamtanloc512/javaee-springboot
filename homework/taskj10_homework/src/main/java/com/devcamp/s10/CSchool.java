package com.devcamp.s10;

import java.util.ArrayList;
import java.util.List;

public class CSchool {
	private String schoolName;
	private List<CClass> classList;
	private List<CPerson> teacherList;
	private List<CPerson> studentList;

	public CSchool() {
		super();
	}

	public CSchool(String schoolName, List<CClass> classList, ArrayList<CPerson> arrayList, List<CPerson> studentList) {
		super();
		this.schoolName = schoolName;
		this.classList = classList;
		this.teacherList = arrayList;
		this.studentList = studentList;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public List<CClass> getClassList() {
		return classList;
	}

	public void setClassList(List<CClass> classList) {
		this.classList = classList;
	}

	public List<CPerson> getTeacherList() {
		return teacherList;
	}

	public void setTeacherList(List<CPerson> teacherList) {
		this.teacherList = teacherList;
	}

	public List<CPerson> getStudentList() {
		return studentList;
	}

	public void setStudentList(List<CPerson> studentList) {
		this.studentList = studentList;
	}

}
