package com.devcamp.s10;

public class CClass {
	private int classCode;
	private String className;
	private CTeacher mainTeacher;
	private String phoneNumberTeacher;

	public CClass() {
		super();
	}

	public CClass(int classCode, String className, CTeacher mainTeacher, String phoneNumberTeacher) {
		super();
		this.classCode = classCode;
		this.className = className;
		this.mainTeacher = mainTeacher;
		this.phoneNumberTeacher = phoneNumberTeacher;
	}

	public int getClassCode() {
		return classCode;
	}

	public void setClassCode(int classCode) {
		this.classCode = classCode;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public CTeacher getMainTeacher() {
		return mainTeacher;
	}

	public void setMainTeacher(CTeacher mainTeacher) {
		this.mainTeacher = mainTeacher;
	}

	public String getphoneNumberTeacher() {
		return phoneNumberTeacher;
	}

	public void setphoneNumberTeacher(String phoneNumberTeacher) {
		this.phoneNumberTeacher = phoneNumberTeacher;
	}

}
