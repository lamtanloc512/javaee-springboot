package com.devcamp.s10;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CController {

	public CSchool getSchool() {
		LocalDateTime birthday = LocalDateTime.now();
		CClass class1A = new CClass(1, "1A", new CTeacher(1, "Lê Bảy", "male", birthday, "Số 1 Lê Duẩn", "0985780716"),
				"0985780716");
		CClass class2A = new CClass(2, "2A",
				new CTeacher(2, "Lê Sáu", "female", birthday, "Số 2 Lê Duẩn", "0985780714"), "0985780714");
		CClass class3A = new CClass(3, "3A", new CTeacher(3, "Lê Năm", "male", birthday, "Số 3 Lê Duẩn", "0985780715"),
				"0985780715");

		CPerson student1 = new CStudent(1, "Nguyễn Văn A", "male", "Số 11 Lê Duẩn", birthday, "086472838");
		CPerson student2 = new CStudent(2, "Nguyễn Văn B", "male", "Số 21 Lê Duẩn", birthday, "096472838");
		CPerson student3 = new CStudent(3, "Nguyễn Văn C", "male", "Số 31 Lê Duẩn", birthday, "097472838");

		CSchool schoolA = new CSchool("Lê Quý Đôn", new ArrayList<CClass>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(class1A);
				add(class2A);
				add(class3A);
			}
		}, new ArrayList<CPerson>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(class1A.getMainTeacher());
				add(class2A.getMainTeacher());
				add(class3A.getMainTeacher());
			}
		}, new ArrayList<CPerson>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(student1);
				add(student2);
				add(student3);
			}
		});

		System.out.println("hello");

		return schoolA;
	}

	@GetMapping("/school")
	public CSchool getSchoolElement() {
		return getSchool();
	}

}
