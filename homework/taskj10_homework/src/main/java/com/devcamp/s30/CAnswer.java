package com.devcamp.s30;

public class CAnswer {
    private int maDapAn;
    private String noiDungDapAn;
    private boolean cauTraLoi;
    private String giaiThichDapAn;

    public CAnswer(int maDapAn, String noiDungDapAn, boolean cauTraLoi, String giaiThichDapAn) {
        this.maDapAn = maDapAn;
        this.noiDungDapAn = noiDungDapAn;
        this.cauTraLoi = cauTraLoi;
        this.giaiThichDapAn = giaiThichDapAn;
    }

    public CAnswer() {
    }

    public int getMaDapAn() {
        return maDapAn;
    }

    public void setMaDapAn(int maDapAn) {
        this.maDapAn = maDapAn;
    }

    public String getNoiDungDapAn() {
        return noiDungDapAn;
    }

    public void setNoiDungDapAn(String noiDungDapAn) {
        this.noiDungDapAn = noiDungDapAn;
    }

    public boolean isCauTraLoi() {
        return cauTraLoi;
    }

    public void setCauTraLoi(boolean cauTraLoi) {
        this.cauTraLoi = cauTraLoi;
    }

    public String getGiaiThichDapAn() {
        return giaiThichDapAn;
    }

    public void setGiaiThichDapAn(String giaiThichDapAn) {
        this.giaiThichDapAn = giaiThichDapAn;
    }
}
