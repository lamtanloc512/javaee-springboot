package com.devcamp.s30;

import java.util.Date;
import java.util.List;

public class CQuestion {
    private int maCauHoi;
    private String noiDungCauHoi;
    private float diemCauHoi;
    private String tenMonHoc;
    private Date ngayTao;
    private List<CAnswer> danhSachDapAn;

    public CQuestion() {
    }

    public CQuestion(int maCauHoi, String noiDungCauHoi, float diemCauHoi, String tenMonHoc, Date ngayTao, List<CAnswer> danhSachDapAn) {
        this.maCauHoi = maCauHoi;
        this.noiDungCauHoi = noiDungCauHoi;
        this.diemCauHoi = diemCauHoi;
        this.tenMonHoc = tenMonHoc;
        this.ngayTao = ngayTao;
        this.danhSachDapAn = danhSachDapAn;
    }

    public int getMaCauHoi() {
        return maCauHoi;
    }

    public void setMaCauHoi(int maCauHoi) {
        this.maCauHoi = maCauHoi;
    }

    public String getNoiDungCauHoi() {
        return noiDungCauHoi;
    }

    public void setNoiDungCauHoi(String noiDungCauHoi) {
        this.noiDungCauHoi = noiDungCauHoi;
    }

    public float getDiemCauHoi() {
        return diemCauHoi;
    }

    public void setDiemCauHoi(float diemCauHoi) {
        this.diemCauHoi = diemCauHoi;
    }

    public String getTenMonHoc() {
        return tenMonHoc;
    }

    public void setTenMonHoc(String tenMonHoc) {
        this.tenMonHoc = tenMonHoc;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public List<CAnswer> getDanhSachDapAn() {
        return danhSachDapAn;
    }

    public void setDanhSachDapAn(List<CAnswer> danhSachDapAn) {
        this.danhSachDapAn = danhSachDapAn;
    }
}
