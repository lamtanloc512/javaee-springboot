package com.devcamp.s30;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CS30Controller {

    @GetMapping("/test")
    public List<CQuestion> getQuestions() {
	List<CQuestion> questionList = new ArrayList<CQuestion>();

	CQuestion question1 = new CQuestion(1, "cau1", 10, "toan", new Date(), new ArrayList<CAnswer>() {
	    /**
	     * 
	     */
	    private static final long serialVersionUID = 1L;

	    {
		add(new CAnswer(1, "Cau A", true, "cau nay dung vi no dung"));
		add(new CAnswer(2, "Cau B", true, "cau nay dung vi no dung"));
		add(new CAnswer(3, "Cau C", false, "cau nay sai vi no sai"));
		add(new CAnswer(4, "Cau D", false, "cau nay sai vi no khong dung"));
	    }
	});
	CQuestion question2 = new CQuestion(2, "cau2", 10, "toan", new Date(), new ArrayList<CAnswer>() {
	    /**
	     * 
	     */
	    private static final long serialVersionUID = 1L;

	    {
		add(new CAnswer(1, "Cau A", true, "cau nay dung vi no dung"));
		add(new CAnswer(2, "Cau B", true, "cau nay dung vi no dung"));
		add(new CAnswer(3, "Cau C", false, "cau nay sai vi no sai"));
		add(new CAnswer(4, "Cau D", false, "cau nay sai vi no khong dung"));
	    }
	});
	CQuestion question3 = new CQuestion(3, "cau3", 10, "vatly", new Date(), new ArrayList<CAnswer>() {
	    /**
	     * 
	     */
	    private static final long serialVersionUID = 1L;

	    {
		add(new CAnswer(1, "Cau A", true, "cau nay dung vi no dung"));
		add(new CAnswer(2, "Cau B", true, "cau nay dung vi no dung"));
		add(new CAnswer(3, "Cau C", false, "cau nay sai vi no sai"));
		add(new CAnswer(4, "Cau D", false, "cau nay sai vi no khong dung"));
	    }
	});
	CQuestion question4 = new CQuestion(4, "cau4", 10, "hoahoc", new Date(), new ArrayList<CAnswer>() {
	    /**
	     * 
	     */
	    private static final long serialVersionUID = 1L;

	    {
		add(new CAnswer(1, "Cau A", true, "cau nay dung vi no dung"));
		add(new CAnswer(2, "Cau B", true, "cau nay dung vi no dung"));
		add(new CAnswer(3, "Cau C", false, "cau nay sai vi no sai"));
		add(new CAnswer(4, "Cau D", false, "cau nay sai vi no khong dung"));
	    }
	});

	questionList.add(question1);
	questionList.add(question2);
	questionList.add(question3);
	questionList.add(question4);

	return questionList;
    }
}
