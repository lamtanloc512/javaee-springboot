package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Taskj10HomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(Taskj10HomeworkApplication.class, args);
	}

}
