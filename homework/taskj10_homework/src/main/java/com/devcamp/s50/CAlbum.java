package com.devcamp.s50;

import java.time.LocalDate;
import java.util.List;

public class CAlbum extends CMedia {
    private List<CImage> images;

    public CAlbum() {
        super();
    }

    public CAlbum(int id, String name, String description, LocalDate ngayTao, List<CImage> images) {
        super(id, name, description, ngayTao);
        this.images = images;
    }

    public CAlbum(List<CImage> images) {
        super();
        this.images = images;
    }

    public List<CImage> getImages() {
        return images;
    }

    public void setImages(List<CImage> images) {
        this.images = images;
    }

    @Override
    public void create() {

    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }

    @Override
    public void search() {

    }

}
