package com.devcamp.s50;

import java.time.LocalDate;

public class CImage extends CMedia {

    public CImage() {
	super();
    }

    public CImage(int id, String name, String description, LocalDate ngayTao) {
	super(id, name, description, ngayTao);
    }

    @Override
    public void create() {

    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }

    @Override
    public void search() {

    }

}
