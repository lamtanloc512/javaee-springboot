package com.devcamp.s50;

import java.util.List;

public class CLibrary {
    private String name;
    private int id;
    private List<CAlbum> albums;

    public CLibrary(String name, int id, List<CAlbum> albums) {
	super();
	this.name = name;
	this.id = id;
	this.albums = albums;
    }

    public CLibrary() {
	super();
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public List<CAlbum> getAlbums() {
	return albums;
    }

    public void setAlbums(List<CAlbum> albums) {
	this.albums = albums;
    }

}
