package com.devcamp.s50;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CS50Controller {

    @GetMapping("/libraries")
    public List<CLibrary> getCLibraries() {
        List<CLibrary> libraries = new ArrayList<CLibrary>();
        CLibrary library1 = new CLibrary("library1", 1, new ArrayList<>() {
            {
                add(new CAlbum(1, "album1", "album1", LocalDate.now(), new ArrayList<CImage>() {
                    {
                        add(new CImage(1, "img1", "img1", LocalDate.now()));
                        add(new CImage(2, "img2", "img2", LocalDate.now()));
                        add(new CImage(3, "img3", "img3", LocalDate.now()));
                        add(new CImage(4, "img4", "img4", LocalDate.now()));
                    }
                }));
                add(new CAlbum(2, "album2", "album2", LocalDate.now(), new ArrayList<CImage>() {
                    {
                        add(new CImage(1, "img1", "img1", LocalDate.now()));
                        add(new CImage(2, "img2", "img2", LocalDate.now()));
                        add(new CImage(3, "img3", "img3", LocalDate.now()));
                        add(new CImage(4, "img4", "img4", LocalDate.now()));
                    }
                }));
                add(new CAlbum(3, "album3", "album3", LocalDate.now(), new ArrayList<CImage>() {
                    {
                        add(new CImage(1, "img1", "img1", LocalDate.now()));
                        add(new CImage(2, "img2", "img2", LocalDate.now()));
                        add(new CImage(3, "img3", "img3", LocalDate.now()));
                        add(new CImage(4, "img4", "img4", LocalDate.now()));
                    }
                }));
                add(new CAlbum(4, "album4", "album4", LocalDate.now(), new ArrayList<CImage>() {
                    {
                        add(new CImage(1, "img1", "img1", LocalDate.now()));
                        add(new CImage(2, "img2", "img2", LocalDate.now()));
                        add(new CImage(3, "img3", "img3", LocalDate.now()));
                        add(new CImage(4, "img4", "img4", LocalDate.now()));
                    }
                }));
            }
        });
        libraries.add(library1);
        return libraries;
    }
}
