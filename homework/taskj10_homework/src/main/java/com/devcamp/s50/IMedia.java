package com.devcamp.s50;

public interface IMedia {
    public void create();

    public void update();

    public void delete();
    
    public void search();
}
