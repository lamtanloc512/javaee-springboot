package com.devcamp.s50;

import java.time.LocalDate;

public abstract class CMedia implements IMedia {
    private int id;
    private String name;
    private String description;
    private LocalDate ngayTao;

    public CMedia() {
	super();
    }

    public CMedia(int id, String name, String description, LocalDate ngayTao) {
	super();
	this.id = id;
	this.name = name;
	this.description = description;
	this.ngayTao = ngayTao;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public LocalDate getNgayTao() {
	return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
	this.ngayTao = ngayTao;
    }

}
