package com.devcamp.s20;

import java.util.List;

public abstract class CDepartment {
	private int departmentId;
	private String departmentName;
	private List<CPerson> staffs;

	public CDepartment(int departmentId, String departmentName, List<CPerson> staffs) {
		super();
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.staffs = staffs;
	}

	public CDepartment() {
		super();
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public List<CPerson> getStaffs() {
		return staffs;
	}

	public void setStaffs(List<CPerson> staffs) {
		this.staffs = staffs;
	}

}
