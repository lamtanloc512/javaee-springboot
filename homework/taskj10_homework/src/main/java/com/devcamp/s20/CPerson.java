package com.devcamp.s20;

public abstract class CPerson {
    private int personId;
    private String personName;
    private String personRole;
    private String personeGender;
    private String personBirthday;
    private String personAddress;
    private String personPhoneNumber;

    public CPerson() {
        super();
    }

    public CPerson(int personId, String personName, String personRole, String personeGender, String personBirthday,
                   String personAddress, String personPhoneNumber) {
        super();
        this.personId = personId;
        this.personName = personName;
        this.personRole = personRole;
        this.personeGender = personeGender;
        this.personBirthday = personBirthday;
        this.personAddress = personAddress;
        this.personPhoneNumber = personPhoneNumber;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonRole() {
        return personRole;
    }

    public void setPersonRole(String personRole) {
        this.personRole = personRole;
    }

    public String getPersoneGender() {
        return personeGender;
    }

    public void setPersoneGender(String personeGender) {
        this.personeGender = personeGender;
    }

    public String getPersonBirthday() {
        return personBirthday;
    }

    public void setPersonBirthday(String personBirthday) {
        this.personBirthday = personBirthday;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonPhoneNumber() {
        return personPhoneNumber;
    }

    public void setPersonPhoneNumber(String personPhoneNumber) {
        this.personPhoneNumber = personPhoneNumber;
    }

}
