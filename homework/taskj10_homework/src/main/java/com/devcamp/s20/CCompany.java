package com.devcamp.s20;

import java.util.List;

public class CCompany {
	private List<CDepartment> departments;
	private List<CPerson> staffList;

	public CCompany(List<CDepartment> departments, List<CPerson> staffList) {
		super();
		this.departments = departments;
		this.staffList = staffList;
	}

	public CCompany() {
		super();
	}

	public List<CDepartment> getDepartments() {
		return departments;
	}

	public void setDepartments(List<CDepartment> departments) {
		this.departments = departments;
	}

	public List<CPerson> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<CPerson> staffList) {
		this.staffList = staffList;
	}

}
