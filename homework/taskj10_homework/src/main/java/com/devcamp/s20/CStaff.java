package com.devcamp.s20;

public class CStaff extends CPerson {
	private int salary;

	public CStaff() {
		super();
	}

	public CStaff(int personId, String personName, String personRole, String personeGender, String personBirthday,
			String personAddress, String personPhoneNumber) {
		super(personId, personName, personRole, personeGender, personBirthday, personAddress, personPhoneNumber);
	}

	public CStaff createCStaff(int salary) {
		CStaff staff = new CStaff(0, "lamtanloc", "manager", "male", "21/01/1993", "119 nguyen tat thanh",
				"0886487293");
		staff.setSalary(salary);
		return staff;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

}
