package com.devcamp.s20;

public class CCar {
	private String brandName;
	private int carId;

	public CCar(String brandName, int carId) {
		super();
		this.brandName = brandName;
		this.carId = carId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

}
