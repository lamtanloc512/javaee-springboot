package com.devcamp.s20;

import java.util.List;

public class CRepaireDepartment extends CDepartment {
	private List<CCar> cars;

	public CRepaireDepartment() {
		super();
	}

	public CRepaireDepartment(int departmentId, String departmentName, List<CPerson> staffs) {
		super(departmentId, departmentName, staffs);
	}

	public List<CCar> getCars() {
		return cars;
	}

	public void setCars(List<CCar> cars) {
		this.cars = cars;
	}

}
