package com.devcamp.s20;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CS20Controller {

    @GetMapping("/company")
    public CCompany getCompany() {

        CPerson staff1 = new CStaff(1, "nguyen van a", "staff", "male", "01/01/1995", "so 1 le van sy", "0886467990");
        CPerson staff2 = new CStaff(2, "nguyen van b", "staff", "male", "02/01/1996", "so 2 le van sy", "0886467980");
        CPerson staff3 = new CStaff(3, "nguyen van c", "staff", "male", "03/01/1997", "so 3 le van sy", "0886467970");
        CPerson worker1 = new CWorker(4, "nguyen van d", "worker", "male", "03/01/1999", "so 5 le van sy", "088644570");

        CPerson createmanager = new CStaff();
        CPerson manager = ((CStaff) createmanager).createCStaff(50);

        return new CCompany(new ArrayList<CDepartment>() {
            private static final long serialVersionUID = 1L;

            {
                add(new CRepaireDepartment(1, "Phòng Sửa Chữa Oto", new ArrayList<CPerson>() {

                    private static final long serialVersionUID = 1L;

                    {
                        add(staff1);
                        add(staff2);
                        add(staff3);
                        add(worker1);
                    }
                }) {
                });
            }

        }, new ArrayList<CPerson>() {
            private static final long serialVersionUID = 1L;

            {
                add(manager);
                add(staff1);
                add(staff2);
                add(staff3);
                add(worker1);
            }
        });
    }

}
