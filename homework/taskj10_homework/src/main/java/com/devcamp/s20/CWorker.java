package com.devcamp.s20;

public class CWorker extends CPerson {
	private int workerSalary;

	public CWorker(int workerSalary) {
		super();
		this.workerSalary = workerSalary;
	}

	public CWorker() {
		super();
	}

	public CWorker(int personId, String personName, String personRole, String personeGender, String personBirthday,
			String personAddress, String personPhoneNumber) {
		super(personId, personName, personRole, personeGender, personBirthday, personAddress, personPhoneNumber);
	}

	public int getWorkerSalary() {
		return workerSalary;
	}

	public void setWorkerSalary(int workerSalary) {
		this.workerSalary = workerSalary;
	}
	
	
}
