package com.devcamp.oop2;

abstract class Shape {
	abstract void draw();
}

class Retangle extends Shape {
	void draw() {
		System.out.println("drawing retangle");
	}
}

class Circle1 extends Shape {
	void draw() {
		System.out.println("drawing circle");
	}
}

class TestAbstraction1 {
	public static void main(String[] args) {
		Shape s = new Circle1();
		s.draw();
	}
}
