package com.devcamp.oop2;

public class Student {
	int rollno;
	String name;
	static String college = "ITS";

	static void change() {
		college = "BBDIT";
	}

	public Student(int rollno, String name) {
		this.rollno = rollno;
		this.name = name;
	}
	
	void display() {
		System.out.println(rollno + " " + name + " " + college);
	}
	
}
