package com.devcamp.oop3;

interface Drawable {
	void draw();
	default void msg() {
		System.out.println("default method");
	}
}

class Retangle	implements Drawable{

	public void draw() {
		System.out.println("draw reactangle");
	}
	
}

class TestInterfaceDefault implements Drawable {

	public static void main(String[] args) {
		Retangle obj = new Retangle();
		obj.draw();
		obj.msg();

	}

	@Override
	public
	void draw() {
		
	}

}
