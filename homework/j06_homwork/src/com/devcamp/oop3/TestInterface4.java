package com.devcamp.oop3;

interface Printable {
	void print();
}

interface Showable {
	void show();
}

class TestInterface4 implements Showable {
	public void print() {
		System.out.println("hello");
	}

	public void show() {
		System.out.println("Welcome");
	}

	public static void main(String[] args) {
		TestInterface4 obj = new TestInterface4();
		obj.print();
		obj.show();
	}

}
