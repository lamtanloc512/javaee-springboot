package com.devcamp.oop;

class OverloadingExample {

	static int add(int a, int b) {
		return a + b;
	}

	static int add(int a, int b, int c) {
		return a + b + c;
	}
	
	public static void main(String[] args) {
		OverloadingExample test = new OverloadingExample();
		System.out.println( test.add(2, 5));
		System.out.println("=======================");
		System.out.println( test.add(2, 5, 10));
	}
}
