package com.devcamp.oop;


class Animal {
	void eat() {
		System.out.println("eating ...");
	}
	
	public static void main(String[] args) {
		Animal nAnimal = new Animal();
		nAnimal.eat();
		
		Animal nAnimal2 = new Dog();
		nAnimal2.eat();
	}
	
}

class Dog extends Animal{
	void eat() {
		System.out.println("eating bread ...");
	}
}