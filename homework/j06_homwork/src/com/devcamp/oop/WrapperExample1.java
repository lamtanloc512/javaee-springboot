package com.devcamp.oop;

public class WrapperExample1 {
	public static void main(String[] args) {

		byte b = 10;
		short s = 20;
		int i = 30;
		long l = 40;
		float f = 50.0F;
		double d = 60.0D;
		char c = 'a';
		boolean b2 = true;

		// autoboxing
		Byte byteobj = b;
		Short shortobj = s;
		Integer integerobj = i;
		Long longobj = l;
		Float floatobj = f;
		Double doubleobj = d;
		Character charobj = c;
		Boolean booleanobj = b2;

		System.out.println("_________________________________________");
		System.out.println("byte obj: " + byteobj);
		System.out.println("short obj: " + shortobj);
		System.out.println("integer obj: " + integerobj);
		System.out.println("long obj: " + longobj);
		System.out.println("float obj: " + floatobj);
		System.out.println("double obj: " + doubleobj);
		System.out.println("char obj: " + charobj);
		System.out.println("boolean obj: " + booleanobj);

		byte bytevalue = byteobj;
		short shortvalue = shortobj;
		int integervalue = integerobj;
		long longvalue = longobj;
		float floatvalue = floatobj;
		double doublevalue = doubleobj;
		char charvalue = charobj;
		boolean boobleanvalue = booleanobj;

		System.out.println("_________________________________________");
		System.out.println("byte value: " + bytevalue);
		System.out.println("short value: " + shortvalue);
		System.out.println("integer value: " + integervalue);
		System.out.println("long value: " + longvalue);
		System.out.println("float value: " + floatvalue);
		System.out.println("double value: " + doublevalue);
		System.out.println("char value: " + charvalue);
		System.out.println("boolean value: " + boobleanvalue);
	}
}
