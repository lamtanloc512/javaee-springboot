package com.devcamp.oop;

public class Student {
	int rollno;
	String name;
	static String college = "ITS";
	
	public Student(int rollno, String name) {
		super();
		this.rollno = rollno;
		this.name = name;
	}
	
	void display() {
		System.out.println(rollno + " " + name + " " + college);
	}
	
}
