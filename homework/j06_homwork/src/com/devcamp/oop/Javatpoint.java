package com.devcamp.oop;

public class Javatpoint {
	private int i;

	public Javatpoint() {

	}

	public Javatpoint(int i) {
		this.i = i;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	@Override
	public String toString() {
		return Integer.toString(i);
	}
	
	public static void main(String[] args) {
		Javatpoint javatpoint = new Javatpoint(10);
		System.out.println(javatpoint);
	}
	
}
