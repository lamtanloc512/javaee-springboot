package com.devcamp.oop;

public class TestStaticMethod {
	public static void main(String[] args) {
		Student s1 = new Student(111, "karan");
		Student s2 = new Student(222, "aryan");
		Student s3 = new Student(333, "sonoo");
		
		s1.display();
		s2.display();
		s3.display();
	}
}
