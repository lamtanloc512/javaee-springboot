package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskJ21CountryRegionCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskJ21CountryRegionCrudApplication.class, args);
	}

}
