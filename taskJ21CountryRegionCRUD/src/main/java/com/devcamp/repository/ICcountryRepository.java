package com.devcamp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CCountry;

public interface ICcountryRepository extends JpaRepository<CCountry, Long> {
	Optional<CCountry> findByCountryCode(String countryCode);
	Optional<CCountry> findByCountryName(String countryName);
}
