package com.devcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CCountry;
import com.devcamp.repository.ICcountryRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
public class CCountryController {

	@Autowired
	private ICcountryRepository countryRepository;

	@GetMapping(path = "/country")
	public ResponseEntity<Object> getCountryByCountryCode(
			@RequestParam(name = "code", required = true) String countryCode) {
		Optional<CCountry> _countryFound = countryRepository.findByCountryCode(countryCode);
		if (_countryFound.isPresent()) {
			return new ResponseEntity<Object>(_countryFound.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(path = "/country/{name}")
	public ResponseEntity<Object> getCountryByCountryName(
			@PathVariable("name") String countryName) {
		Optional<CCountry> _countryFound = countryRepository.findByCountryName(countryName);
		if (_countryFound.isPresent()) {
			return new ResponseEntity<Object>(_countryFound.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/countries")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newCountry = new CCountry();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Country: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/country/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/country/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/countries")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}

}
