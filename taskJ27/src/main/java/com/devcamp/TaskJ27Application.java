package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskJ27Application {

	public static void main(String[] args) {
		SpringApplication.run(TaskJ27Application.class, args);
	}

}
