package com.devcamp.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.Employees;

public interface EmployeeRepository extends JpaRepository<Employees, Long> {
	// tìm kiếm theo tên firstnam hoặc lastname
	@Query(value = "SELECT * FROM employees WHERE full_name LIKE %?1%", nativeQuery = true)
	List<Employees> getEmpoyeeByFirstNameLastName(String firstNameOrLastName);

	// tìm kiếm theo city hoặc state
	@Query(value = "SELECT * FROM employees WHERE city LIKE %?1% OR state LIKE %?1%", nativeQuery = true)
	List<Employees> getCustomerByCityOrState(String cityOrState, Pageable pageable);

	// tìm kiếm theo country
	@Query(value = "SELECT * FROM employees WHERE country LIKE %?1% ORDER BY full_name DESC", nativeQuery = true)
	List<Employees> getCustomerByCountry(String country, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "UPDATE employees SET country = :country WHERE country is null", nativeQuery = true)
	int updatePhanTram(@Param("country") String country);
}
