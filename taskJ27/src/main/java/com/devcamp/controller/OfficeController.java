package com.devcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.entity.Offices;
import com.devcamp.service.OfficeService;

@RestController
@RequestMapping(path = "/offices")
@CrossOrigin(value = "*", maxAge = -1)
public class OfficeController {

	@Autowired
	OfficeService officeService;

	@GetMapping("/all")
	public ResponseEntity<Object> getAllOffices() {
		try {
			List<Offices> officeList = officeService.getOffices();
			return new ResponseEntity<Object>(officeList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/create")
	public ResponseEntity<Object> createOffice(Offices offices) {
		try {
			officeService.createOffice(offices);
			return new ResponseEntity<Object>("Created", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
		}
	}

}
