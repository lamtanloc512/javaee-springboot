package com.devcamp.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product_lines")
public class ProductLines {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "INT(10)")
	private Long id;

	@Column(name = "product_line", columnDefinition = "VARCHAR(50)")
	private String product_line;

	@Column(name = "description", columnDefinition = "VARCHAR(2500)")
	private String description;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Products.class, mappedBy = "productLineId")
	@JsonIgnore
	private Set<Products> products;

	/**
	 * @param id
	 * @param product_line
	 * @param description
	 * @param products
	 */
	public ProductLines(Long id, String product_line, String description, Set<Products> products) {
		super();
		this.id = id;
		this.product_line = product_line;
		this.description = description;
		this.products = products;
	}

	/**
	 * 
	 */
	public ProductLines() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the product_line
	 */
	public String getProduct_line() {
		return product_line;
	}

	/**
	 * @param product_line the product_line to set
	 */
	public void setProduct_line(String product_line) {
		this.product_line = product_line;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the products
	 */
	public Set<Products> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(Set<Products> products) {
		this.products = products;
	}

}
