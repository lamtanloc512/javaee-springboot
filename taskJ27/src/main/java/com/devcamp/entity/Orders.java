package com.devcamp.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "_orders")
public class Orders {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "INT(10)")
	private Long id;

	@Column(name = "order_date", columnDefinition = "DATETIME")
	private Date order_date;

	@Column(name = "required_date", columnDefinition = "DATETIME")
	private Date required_date;

	@Column(name = "shipped_date", columnDefinition = "DATETIME")
	private Date shipped_date;

	@Column(name = "status", columnDefinition = "VARCHAR(50)")
	private String status;

	@Column(name = "comments", columnDefinition = "VARCHAR(255)")
	private String comments;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id", nullable = false)
	@JsonIgnore
	private Customers customer;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "order", fetch = FetchType.LAZY, targetEntity = OrderDetails.class)
	@JsonIgnore
	private Set<OrderDetails> orderDetails;

	/**
	 * 
	 */
	public Orders() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param order_date
	 * @param required_date
	 * @param shipped_date
	 * @param status
	 * @param comments
	 * @param customer
	 * @param orderDetails
	 */
	public Orders(Long id, Date order_date, Date required_date, Date shipped_date, String status, String comments,
			Customers customer, Set<OrderDetails> orderDetails) {
		super();
		this.id = id;
		this.order_date = order_date;
		this.required_date = required_date;
		this.shipped_date = shipped_date;
		this.status = status;
		this.comments = comments;
		this.customer = customer;
		this.orderDetails = orderDetails;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the order_date
	 */
	public Date getOrder_date() {
		return order_date;
	}

	/**
	 * @param date the order_date to set
	 */
	public void setOrder_date(Date date) {
		this.order_date = date;
	}

	/**
	 * @return the required_date
	 */
	public Date getRequired_date() {
		return required_date;
	}

	/**
	 * @param required_date the required_date to set
	 */
	public void setRequired_date(Date required_date) {
		this.required_date = required_date;
	}

	/**
	 * @return the shipped_date
	 */
	public Date getShipped_date() {
		return shipped_date;
	}

	/**
	 * @param shipped_date the shipped_date to set
	 */
	public void setShipped_date(Date shipped_date) {
		this.shipped_date = shipped_date;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the customer
	 */
	public Customers getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customers customer) {
		this.customer = customer;
	}

	/**
	 * @return the orderDetails
	 */
	public Set<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	/**
	 * @param orderDetails the orderDetails to set
	 */
	public void setOrderDetails(Set<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}

}
