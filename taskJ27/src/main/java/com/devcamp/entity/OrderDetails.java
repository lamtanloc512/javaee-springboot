package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "order_details")
public class OrderDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "INT(10)")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", nullable = false)
	@JsonIgnore
	private Orders order;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	@JsonIgnore
	private Products productId;

	@Column(name = "quantity_order", columnDefinition = "INT(10)")
	private int quantity_order;

	@Column(name = "price_each", columnDefinition = "DECIMAL(10,2)")
	private double price_each;

	/**
	 * 
	 */
	public OrderDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the order
	 */
	public Orders getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Orders order) {
		this.order = order;
	}

	/**
	 * @return the productId
	 */
	public Products getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Products productId) {
		this.productId = productId;
	}

	/**
	 * @return the quantity_order
	 */
	public int getQuantity_order() {
		return quantity_order;
	}

	/**
	 * @param quantity_order the quantity_order to set
	 */
	public void setQuantity_order(int quantity_order) {
		this.quantity_order = quantity_order;
	}

	/**
	 * @return the price_each
	 */
	public double getPrice_each() {
		return price_each;
	}

	/**
	 * @param price_each the price_each to set
	 */
	public void setPrice_each(double price_each) {
		this.price_each = price_each;
	}

}
