package com.devcamp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "payments")
public class Payments {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "INT(10)")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id", nullable = false)
	@JsonIgnore
	private Customers customer;

	@Column(name = "check_number", columnDefinition = "VARCHAR(50)")
	private String check_number;

	@Column(name = "payment_date", columnDefinition = "DATETIME")
	private Date payment_date;

	@Column(name = "ammount", columnDefinition = "Decimal(10,2)")
	private double ammount;

	/**
	 * 
	 */
	public Payments() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the check_number
	 */
	public String getCheck_number() {
		return check_number;
	}

	/**
	 * @param check_number the check_number to set
	 */
	public void setCheck_number(String check_number) {
		this.check_number = check_number;
	}

	/**
	 * @return the payment_date
	 */
	public Date getPayment_date() {
		return payment_date;
	}

	/**
	 * @param date the payment_date to set
	 */
	public void setPayment_date(Date date) {
		this.payment_date = date;
	}

	/**
	 * @return the ammount
	 */
	public double getAmmount() {
		return ammount;
	}

	/**
	 * @param ammount the ammount to set
	 */
	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}

	/**
	 * @return the customer
	 */
	public Customers getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customers customer) {
		this.customer = customer;
	}

}
