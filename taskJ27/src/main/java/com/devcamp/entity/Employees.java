package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Employees {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "INT(10)")
	private Long id;

	@Column(name = "last_name", columnDefinition = "VARCHAR(50)")
	private String last_name;

	@Column(name = "first_name", columnDefinition = "VARCHAR(50)")
	private String first_name;

	@Column(name = "extension", columnDefinition = "VARCHAR(50)")
	private String extension;

	@Column(name = "email", columnDefinition = "VARCHAR(50)")
	private String email;

	@Column(name = "office_code", columnDefinition = "int(10)")
	private Integer office_code;

	@Column(name = "report_to", columnDefinition = "INT(10)")
	private Integer report_to;

	@Column(name = "job_title", columnDefinition = "VARCHAR(50)")
	private String job_title;

	public Employees() {
	}

	/**
	 * @param id
	 * @param last_name
	 * @param first_name
	 * @param extension
	 * @param email
	 * @param office_code
	 * @param report_to
	 * @param job_title
	 */
	public Employees(Long id, String last_name, String first_name, String extension, String email, Integer office_code,
			Integer report_to, String job_title) {
		super();
		this.id = id;
		this.last_name = last_name;
		this.first_name = first_name;
		this.extension = extension;
		this.email = email;
		this.office_code = office_code;
		this.report_to = report_to;
		this.job_title = job_title;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the office_code
	 */
	public Integer getOffice_code() {
		return office_code;
	}

	/**
	 * @param office_code the office_code to set
	 */
	public void setOffice_code(Integer office_code) {
		this.office_code = office_code;
	}

	/**
	 * @return the report_to
	 */
	public Integer getReport_to() {
		return report_to;
	}

	/**
	 * @param report_to the report_to to set
	 */
	public void setReport_to(Integer report_to) {
		this.report_to = report_to;
	}

	/**
	 * @return the job_title
	 */
	public String getJob_title() {
		return job_title;
	}

	/**
	 * @param job_title the job_title to set
	 */
	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}

}
