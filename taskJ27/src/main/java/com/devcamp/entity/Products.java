package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "products")
public class Products {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "INT(10)")
	private Long id;

	@Column(name = "product_code", columnDefinition = "VARCHAR(50)")
	private String product_code;

	@Column(name = "product_name", columnDefinition = "VARCHAR(255)")
	private String product_name;

	@Column(name = "product_description", columnDefinition = "VARCHAR(2500)")
	private String product_description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_line_id", nullable = false)
	@JsonIgnore
	private ProductLines productLineId;

	@Column(name = "product_scale", columnDefinition = "VARCHAR(50)")
	private String product_scale;

	@Column(name = "product_vendor", columnDefinition = "VARCHAR(50)")
	private String product_vendor;

	@Column(name = "quantity_in_stock", columnDefinition = "INT(10)")
	private int quantity_in_stock;

	@Column(name = "buy_price", columnDefinition = "DECIMAL(10,2)")
	private double buy_price;

	/**
	 * 
	 */
	public Products() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param product_code
	 * @param product_name
	 * @param product_description
	 * @param productLineId
	 * @param product_scale
	 * @param product_vendor
	 * @param quantity_in_stock
	 * @param buy_price
	 */
	public Products(Long id, String product_code, String product_name, String product_description,
			ProductLines productLineId, String product_scale, String product_vendor, int quantity_in_stock,
			double buy_price) {
		super();
		this.id = id;
		this.product_code = product_code;
		this.product_name = product_name;
		this.product_description = product_description;
		this.productLineId = productLineId;
		this.product_scale = product_scale;
		this.product_vendor = product_vendor;
		this.quantity_in_stock = quantity_in_stock;
		this.buy_price = buy_price;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the product_code
	 */
	public String getProduct_code() {
		return product_code;
	}

	/**
	 * @param product_code the product_code to set
	 */
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	/**
	 * @return the product_name
	 */
	public String getProduct_name() {
		return product_name;
	}

	/**
	 * @param product_name the product_name to set
	 */
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	/**
	 * @return the product_description
	 */
	public String getProduct_description() {
		return product_description;
	}

	/**
	 * @param product_description the product_description to set
	 */
	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}

	/**
	 * @return the productLineId
	 */
	public ProductLines getProductLineId() {
		return productLineId;
	}

	/**
	 * @param productLineId the productLineId to set
	 */
	public void setProductLineId(ProductLines productLineId) {
		this.productLineId = productLineId;
	}

	/**
	 * @return the product_scale
	 */
	public String getProduct_scale() {
		return product_scale;
	}

	/**
	 * @param product_scale the product_scale to set
	 */
	public void setProduct_scale(String product_scale) {
		this.product_scale = product_scale;
	}

	/**
	 * @return the product_vendor
	 */
	public String getProduct_vendor() {
		return product_vendor;
	}

	/**
	 * @param product_vendor the product_vendor to set
	 */
	public void setProduct_vendor(String product_vendor) {
		this.product_vendor = product_vendor;
	}

	/**
	 * @return the quantity_in_stock
	 */
	public int getQuantity_in_stock() {
		return quantity_in_stock;
	}

	/**
	 * @param quantity_in_stock the quantity_in_stock to set
	 */
	public void setQuantity_in_stock(int quantity_in_stock) {
		this.quantity_in_stock = quantity_in_stock;
	}

	/**
	 * @return the buy_price
	 */
	public double getBuy_price() {
		return buy_price;
	}

	/**
	 * @param buy_price the buy_price to set
	 */
	public void setBuy_price(double buy_price) {
		this.buy_price = buy_price;
	}

}
