package com.devcamp.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.Payments;
import com.devcamp.repository.PaymentsRepository;

@Service
@Transactional
public class PaymentService {

	@Autowired
	PaymentsRepository paymentsRepository;

	public List<Payments> getPayments() {

		return paymentsRepository.findAll();

	}

	public Payments createPayment(Payments payments) {
		Payments _newPayment = new Payments();
		_newPayment.setAmmount(payments.getAmmount());
		_newPayment.setCheck_number(payments.getCheck_number());
		_newPayment.setCustomer(payments.getCustomer());
		_newPayment.setPayment_date(new Date());

		return paymentsRepository.save(_newPayment);
	}

	public Payments updatePayment(Payments payments) {
		Optional<Payments> _paymentDataFound = paymentsRepository.findById(payments.getId());
		Payments _paymentFound = _paymentDataFound.get();
		if (_paymentDataFound.isPresent()) {
			_paymentFound.setAmmount(payments.getAmmount());
			_paymentFound.setCheck_number(payments.getCheck_number());
			_paymentFound.setCustomer(payments.getCustomer());
			_paymentFound.setPayment_date(new Date());
			return paymentsRepository.save(_paymentFound);
		} else {
			return null;
		}

	}

	public void deletePayment(Long paymentId) {
		paymentsRepository.deleteById(paymentId);
		
	}
}
