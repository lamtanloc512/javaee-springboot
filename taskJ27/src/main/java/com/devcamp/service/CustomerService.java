package com.devcamp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.Customers;
import com.devcamp.repository.CustomersRepository;

@Service
@Transactional
public class CustomerService {

	@Autowired
	CustomersRepository customersRepository;

	public List<Customers> getCustomers() {
		return customersRepository.findAll();
	}

	public Customers createCustomers(Customers customers) {
		Customers _newCustomers = new Customers();
		_newCustomers.setAddress(customers.getAddress());
		_newCustomers.setCity(customers.getCity());
		_newCustomers.setCountry(customers.getCountry());
		_newCustomers.setCreditLimit(customers.getCreditLimit());
		_newCustomers.setFirstName(customers.getFirstName());
		_newCustomers.setLastName(customers.getLastName());
		_newCustomers.setPhoneNumber(customers.getPhoneNumber());
		_newCustomers.setPostalCode(customers.getPostalCode());
		_newCustomers.setSalesRepEmployeeNumber(customers.getSalesRepEmployeeNumber());
		_newCustomers.setState(customers.getState());
		return customersRepository.save(_newCustomers);
	}

	public Customers updateCustomers(Customers customers) {

		Optional<Customers> _customerDataFound = customersRepository.findById(customers.getId());
		if (_customerDataFound.isPresent()) {
			Customers _customerFound = _customerDataFound.get();
			_customerFound.setAddress(customers.getAddress());
			_customerFound.setCity(customers.getCity());
			_customerFound.setCountry(customers.getCountry());
			_customerFound.setCreditLimit(customers.getCreditLimit());
			_customerFound.setFirstName(customers.getFirstName());
			_customerFound.setLastName(customers.getLastName());
			_customerFound.setPhoneNumber(customers.getPhoneNumber());
			_customerFound.setPostalCode(customers.getPostalCode());
			_customerFound.setSalesRepEmployeeNumber(customers.getSalesRepEmployeeNumber());
			_customerFound.setState(customers.getState());
			return customersRepository.save(_customerFound);
		} else {
			return null;
		}

	}

	public void deleteCustomer(Long id) {
		customersRepository.deleteById(id);
	}

}
