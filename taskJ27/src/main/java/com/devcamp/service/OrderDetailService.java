package com.devcamp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.OrderDetails;
import com.devcamp.repository.OrderDetailsRepository;

@Service
@Transactional
public class OrderDetailService {

	@Autowired
	OrderDetailsRepository orderDetailsRepository;

	public List<OrderDetails> getOrderDetails() {
		return orderDetailsRepository.findAll();
	}

	public OrderDetails createOrderDetails(OrderDetails orderDetails) {
		OrderDetails _newOrderDetails = new OrderDetails();
		_newOrderDetails.setOrder(orderDetails.getOrder());
		_newOrderDetails.setPrice_each(orderDetails.getPrice_each());
		_newOrderDetails.setProductId(orderDetails.getProductId());
		_newOrderDetails.setQuantity_order(orderDetails.getQuantity_order());
		return orderDetailsRepository.save(_newOrderDetails);
	}

	public OrderDetails updateOrderDetails(OrderDetails orderDetails) {
		Optional<OrderDetails> _orderDetailDataFound = orderDetailsRepository.findById(orderDetails.getId());
		if (_orderDetailDataFound.isPresent()) {
			OrderDetails _orderDetailFound = _orderDetailDataFound.get();
			_orderDetailFound.setOrder(orderDetails.getOrder());
			_orderDetailFound.setPrice_each(orderDetails.getPrice_each());
			_orderDetailFound.setProductId(orderDetails.getProductId());
			_orderDetailFound.setQuantity_order(orderDetails.getQuantity_order());
			return orderDetailsRepository.save(_orderDetailFound);
		} else {
			return null;
		}

	}

	public void deleteOrderDetail(Long id) {
		orderDetailsRepository.deleteById(id);
	}

}
