package com.devcamp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.Employees;
import com.devcamp.repository.EmployeeRepository;

@Service
@Transactional
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	public List<Employees> getEmployees() {
		return employeeRepository.findAll();
	}

	public Employees createEmployees(Employees employees) {
		Employees _newEmployees = new Employees();
		_newEmployees.setEmail(employees.getEmail());
		_newEmployees.setExtension(employees.getExtension());
		_newEmployees.setFirst_name(employees.getFirst_name());
		_newEmployees.setJob_title(employees.getJob_title());
		_newEmployees.setLast_name(employees.getLast_name());
		_newEmployees.setOffice_code(employees.getOffice_code());
		_newEmployees.setReport_to(employees.getOffice_code());
		return employeeRepository.save(_newEmployees);

	}

	public Employees updateEmployees(Employees employees) {
		Optional<Employees> _employeeDataFound = employeeRepository.findById(employees.getId());
		Employees _employeeFound = _employeeDataFound.get();
		_employeeFound.setEmail(employees.getEmail());
		_employeeFound.setExtension(employees.getExtension());
		_employeeFound.setFirst_name(employees.getFirst_name());
		_employeeFound.setJob_title(employees.getJob_title());
		_employeeFound.setLast_name(employees.getLast_name());
		_employeeFound.setOffice_code(employees.getOffice_code());
		_employeeFound.setReport_to(employees.getOffice_code());

		return employeeRepository.save(_employeeFound);
	}

	public void deleteEmployee(Long id) throws Throwable {
		try {
			employeeRepository.deleteById(id);
		} catch (Exception e) {
			throw e.getCause().getCause().getCause();
		}
	}

}
