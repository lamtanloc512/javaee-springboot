package com.devcamp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.Offices;
import com.devcamp.repository.OfficesRepository;

@Service
@Transactional
public class OfficeService {

	@Autowired
	OfficesRepository officesRepository;

	public List<Offices> getOffices() {
		try {
			return officesRepository.findAll();
		} catch (Exception e) {
			return null;
		}
	}

	public void createOffice(Offices offices) {
		try {
			Offices _newOffice = new Offices();
			_newOffice.setAddress_line(offices.getAddress_line());
			_newOffice.setCity(offices.getCity());
			_newOffice.setCountry(offices.getCountry());
			_newOffice.setPhone(offices.getPhone());
			_newOffice.setState(offices.getState());
			_newOffice.setTerritory(offices.getTerritory());

			officesRepository.save(_newOffice);

		} catch (Exception e) {
			System.out.println("error" + e.getCause().getCause().getCause());
		}
	}

	public void updateOffice(Offices officeUpdate) {
		Optional<Offices> _officeDataFound = officesRepository.findById(officeUpdate.getId());

		if (_officeDataFound.isPresent()) {
			Offices _officeFound = _officeDataFound.get();
			try {
				_officeFound.setAddress_line(officeUpdate.getAddress_line());
				_officeFound.setCity(officeUpdate.getCity());
				_officeFound.setCountry(officeUpdate.getCountry());
				_officeFound.setPhone(officeUpdate.getPhone());
				_officeFound.setState(officeUpdate.getState());
				_officeFound.setTerritory(officeUpdate.getTerritory());
				officesRepository.save(_officeFound);
			} catch (Exception e) {
				System.out.println("error" + e.getCause().getCause().getCause());
			}
		}

	}

	public void deleteOffice(Long officeId) {
		try {
			officesRepository.deleteById(officeId);
		} catch (Exception e) {
			System.out.println("This is error" + e.getCause().getCause().getMessage());
		}
	}

}
