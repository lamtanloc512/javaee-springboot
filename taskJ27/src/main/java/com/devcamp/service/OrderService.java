package com.devcamp.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.Orders;
import com.devcamp.repository.OrdersRepository;

@Service
@Transactional
public class OrderService {

	@Autowired
	OrdersRepository ordersRepository;

	public List<Orders> getOrders() {

		return ordersRepository.findAll();
	}

	public Orders createOrder(Orders orders) {
		Orders _newOrders = new Orders();
		_newOrders.setComments(orders.getComments());
		_newOrders.setCustomer(orders.getCustomer());
		_newOrders.setOrder_date(new Date());
		_newOrders.setOrderDetails(orders.getOrderDetails());
		_newOrders.setRequired_date(new Date());
		_newOrders.setShipped_date(new Date());
		_newOrders.setStatus(orders.getStatus());
		return ordersRepository.save(_newOrders);
	}

	public Orders updateOrder(Orders orders) {
		Optional<Orders> _orderDataFound = ordersRepository.findById(orders.getId());
		if (_orderDataFound.isPresent()) {
			Orders _orderFoundElement = _orderDataFound.get();
			_orderFoundElement.setComments(orders.getComments());
			_orderFoundElement.setCustomer(orders.getCustomer());
			_orderFoundElement.setOrderDetails(orders.getOrderDetails());
			_orderFoundElement.setRequired_date(orders.getOrder_date());
			_orderFoundElement.setShipped_date(orders.getShipped_date());
			_orderFoundElement.setStatus(orders.getStatus());
			return ordersRepository.save(_orderFoundElement);
		} else {
			return null;
		}

	}

	public void deleteOrder(Long id) {
		ordersRepository.deleteById(id);
	}

}
