package com.devcamp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.ProductLines;
import com.devcamp.repository.ProductLinesRepository;

@Service
@Transactional
public class ProductLineService {

	@Autowired
	ProductLinesRepository productLinesRepository;

	public List<ProductLines> getProductLine() {
		return productLinesRepository.findAll();
	}

	public ProductLines creaateProductLine(ProductLines productLines) {
		ProductLines _newProductLine = new ProductLines();
		_newProductLine.setDescription(productLines.getDescription());
		_newProductLine.setProduct_line(productLines.getProduct_line());
		_newProductLine.setProducts(productLines.getProducts());
		return productLinesRepository.save(_newProductLine);
	}

	public ProductLines updateProductLine(ProductLines productLines) {
		Optional<ProductLines> _productLineDataFound = productLinesRepository.findById(productLines.getId());
		if (_productLineDataFound.isPresent()) {
			ProductLines _productLineFound = _productLineDataFound.get();
			_productLineFound.setDescription(productLines.getDescription());
			_productLineFound.setProduct_line(productLines.getProduct_line());
			_productLineFound.setProducts(productLines.getProducts());
			return productLinesRepository.save(_productLineFound);
		} else {
			return null;
		}

	}

	public void deleteProductLine(Long id) {
		productLinesRepository.deleteById(id);
	}

}
