package com.devcamp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.entity.Products;
import com.devcamp.repository.ProductsRepository;

@Service
@Transactional
public class ProductService {

	@Autowired
	ProductsRepository productsRepository;

	public List<Products> getProducts() {
		return productsRepository.findAll();
	}

	public Products createProducts(Products products) {
		Products _newProducts = new Products();
		_newProducts.setBuy_price(products.getBuy_price());
		_newProducts.setProduct_code(products.getProduct_code());
		_newProducts.setProduct_description(products.getProduct_description());
		_newProducts.setProduct_name(products.getProduct_name());
		_newProducts.setProduct_scale(products.getProduct_scale());
		_newProducts.setProductLineId(products.getProductLineId());
		_newProducts.setQuantity_in_stock(products.getQuantity_in_stock());
		_newProducts.setProduct_vendor(products.getProduct_vendor());
		return productsRepository.save(_newProducts);

	}

	public Products updateProducts(Products products) {
		Optional<Products> _productDataFound = productsRepository.findById(products.getId());
		if (_productDataFound.isPresent()) {
			Products _productFound = _productDataFound.get();
			_productFound.setBuy_price(products.getBuy_price());
			_productFound.setProduct_code(products.getProduct_code());
			_productFound.setProduct_description(products.getProduct_description());
			_productFound.setProduct_name(products.getProduct_name());
			_productFound.setProduct_scale(products.getProduct_scale());
			_productFound.setProductLineId(products.getProductLineId());
			_productFound.setQuantity_in_stock(products.getQuantity_in_stock());
			_productFound.setProduct_vendor(products.getProduct_vendor());
			return productsRepository.save(_productFound);
		} else {
			return null;
		}
	}

	public void deleteProduct(Long id) {
		productsRepository.deleteById(id);
	}

}
