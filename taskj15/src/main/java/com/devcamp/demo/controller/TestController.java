package com.devcamp.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.demo.model.User;
import com.devcamp.demo.respository.IUserRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
public class TestController {

	@Autowired
	IUserRepository userRepository;

	@GetMapping(path = "/users")
	public Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}

	@PostMapping(path = "/user/add-new")
	public ResponseEntity<String> createUser(@RequestBody User user) {
		Iterable<User> listUser = userRepository.findAll();
		try {
			listUser.forEach(userElement -> {
				if (userElement.getId() != user.getId()) {
					userRepository.save(user);
				}
			});
			return new ResponseEntity<>(new String("save"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new String("Cannot save"), HttpStatus.BAD_REQUEST);
		}
	}

}
