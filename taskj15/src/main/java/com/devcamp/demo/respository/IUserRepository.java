package com.devcamp.demo.respository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.devcamp.demo.model.User;


public interface IUserRepository extends PagingAndSortingRepository<User, Long> {

}
