package com.devcamp;

public class CDPlayer implements IPlayer {
	private String playerType;

	public CDPlayer(String playerType) {
		super();
		this.playerType = playerType;
	}

	@Override
	public String getPlay() {
		return null;
	}

	@Override
	public String getStop() {
		return null;
	}

	@Override
	public String getPause() {
		return null;
	}

	@Override
	public String getReverse() {
		return null;
	}

	public String getPlayerType() {
		return playerType;
	}

	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}

}
