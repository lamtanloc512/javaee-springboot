package com.devcamp.s50_30;

public abstract class Ball implements ITossable {
	private String brandName;

	public Ball() {
		super();
	}

	public Ball(String brandName) {
		super();
		this.brandName = brandName;
	}

	public abstract String bounce();

	public abstract String toss();

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

}
