package com.devcamp;

import java.util.ArrayList;

import com.devcamp.s50_30.Ball;

public abstract class Person implements IAnimal, ILive {
	private int age;
	private String gender;
	private String name;
	private Address address;
	private ArrayList<Animals> listPet;
	private PersonType personType;
	private ArrayList<IPlayer> listPlay;
	private ArrayList<Ball> listBall;

	public Person(int age, String gender, String name, Address address, ArrayList<Animals> listPet,
			PersonType personType, ArrayList<IPlayer> listPlay) {
		super();
		this.age = age;
		this.gender = gender;
		this.name = name;
		this.address = address;
		this.listPet = listPet;
		this.personType = personType;
		this.listPlay = listPlay;
	}

	public Person() {
		super();
		this.age = 20;
		this.gender = "male";
		this.name = "Nguyễn Văn Nam";
		this.address = new Address();
		this.listPet = new ArrayList<Animals>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(new Duck());
				add(new Fish(4, "male", 25, true));
				add(new Zebra());
			}
		};
		this.setListPlay(new ArrayList<IPlayer>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(new DVDPlayer("dvdplayer"));
				add(new CDPlayer("cdplayer"));
				add(new TapePlayer("tapeplayer"));
			}
		});
	}

	public Person(int age, String gender, String name, Address address) {
		super();
		this.age = age;
		this.gender = gender;
		this.name = name;
		this.address = address;
		this.listPet = new ArrayList<Animals>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(new Duck());
				add(new Fish(4, "male", 25, true));
				add(new Zebra());
			}
		};
	}

	public Person(int age, String gender, String name, Address address, ArrayList<Animals> listPet) {
		super();
		this.age = age;
		this.gender = gender;
		this.name = name;
		this.address = address;
		this.listPet = listPet;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ArrayList<Animals> getListPet() {
		return listPet;
	}

	public void setListPet(ArrayList<Animals> listPet) {
		this.listPet = listPet;
	}

	public PersonType getPersonType() {
		return personType;
	}

	public void setPersonType(PersonType personType) {
		this.personType = personType;
	}

	public abstract void eat();

	@Override
	public void animalSound() {
		System.out.println("Person sound ...");
	}

	@Override
	public void sleep() {
		System.out.println("Person sleeping ...");
	}

	public ArrayList<IPlayer> getListPlay() {
		return listPlay;
	}

	public void setListPlay(ArrayList<IPlayer> listPlay) {
		this.listPlay = listPlay;
	}

	public ArrayList<Ball> getListBall() {
		return listBall;
	}

	public void setListBall(ArrayList<Ball> listBall) {
		this.listBall = listBall;
	}

}
