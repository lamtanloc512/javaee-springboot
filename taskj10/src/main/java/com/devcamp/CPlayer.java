package com.devcamp;

public abstract class CPlayer {
	private PlayerType playerType;

	public PlayerType getPlayerType() {
		return playerType;
	}

	public void setPlayerType(PlayerType playerType) {
		this.playerType = playerType;
	}
}
