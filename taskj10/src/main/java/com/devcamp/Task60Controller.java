package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50_30.Ball;
import com.devcamp.s50_30.BaseBall;
import com.devcamp.s50_30.FootBall;

@RestController
public class Task60Controller {

	public ArrayList<Person> getPersons() {
		Address address1 = new Address("Thái Hà", "Hà Nội", "Việt Nam", 100000);
		Address address2 = new Address("Ba Đình", "Hà Nội", "Việt Nam", 100000);

		Professor professor1 = new Professor(30, "male", "Trần Văn Thắng", address1);
		Professor professor2 = new Professor(35, "male", "Nguyễn Văn Ba", address2, 35000000);

		Subject subject1 = new Subject();
		Subject subject2 = new Subject("Physical", 2, professor1);
		Subject subject3 = new Subject("HTML", 3, professor2);

		IPlayer cdPlayer = new CDPlayer("cdplayer");
		IPlayer dvdPlayer = new DVDPlayer("dvdplayer");
		IPlayer tapePlayer = new TapePlayer("tapeplayer");

		Ball football = new FootBall("Nike");
		Ball baseball = new BaseBall("Adidas");

		ArrayList<Person> lstStudents = new ArrayList<Person>();

		Student student1 = new Student(1, new ArrayList<Subject>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2000591334506466440L;

			{
				add(new Subject());
				add(new Subject("Physical", 2, new Professor(30, "male", "Trần Văn Thắng",
						new Address("Thái Hà", "Hà Nội", "Việt Nam", 100000), 10000)));
			}
		});
		student1.setListPlay(new ArrayList<IPlayer>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(cdPlayer);
				add(dvdPlayer);
			}

		});

		student1.setListBall(new ArrayList<Ball>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(football);
				add(baseball);
			}
		});

		Student student2 = new Student(2, "Lê Văn Thắng", new ArrayList<Subject>(Arrays.asList(subject1, subject2)));
		student2.setListBall(new ArrayList<Ball>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(baseball);
			}
		});
		Student student3 = new Student(3, "Trần Thị Thủy", new ArrayList<Subject>());
		student3.setListSubject(new ArrayList<Subject>(Arrays.asList(subject1, subject2, subject3)));
		student3.setListPlay(new ArrayList<IPlayer>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(cdPlayer);
			}

		});
		student3.setListBall(new ArrayList<Ball>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(football);
			}

		});

		Student student4 = new Student(4, new ArrayList<Subject>(Arrays.asList(subject1, subject2)),
				new ArrayList<Animals>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 2012650563989492710L;

					{
						add(new Duck(1, "male", "yellow"));
						add(new Fish());
						add(new Zebra());
					}
				});
		student4.setListPlay(new ArrayList<IPlayer>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(cdPlayer);
				add(tapePlayer);
			}

		});

		student4.setListBall(new ArrayList<Ball>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(football);
				add(baseball);
			}

		});
//		student1.sleep();
//		student1.animalSound();
//		student1.gotoShool();
//		student1.gotoShop();
//		student1.play();
//
//		professor1.sleep();
//		professor1.animalSound();
//		professor1.gotoShool();
//		professor1.gotoShop();
//		professor1.play();

		ArrayList<Animals> listAnimals = new ArrayList<Animals>();
		listAnimals.add(new Duck());
		listAnimals.add(new Fish());
		listAnimals.add(new Zebra());
		student2.setListPet(listAnimals);
		student1.setListPet(listAnimals);

		lstStudents.add(student1);
		lstStudents.add(student2);
		lstStudents.add(student3);
		lstStudents.add(student4);
		lstStudents.add(new Worker(25, "male", "Lê Nam", address2));
		lstStudents.add(new Professor(45, "male", "Nguyễn Mạnh Tường", address1));
		lstStudents.add(professor1);

		return lstStudents;
	}

//	@CrossOrigin
//	@GetMapping("/listPerson") // 1-worker, 2-student, 3-professor
//	public ArrayList<Person> getListPerson(@RequestParam(value = "type", defaultValue = "0") Integer type,
//			@RequestParam(value = "player", defaultValue = "0") Integer player,
//			@RequestParam(name = "balltype", defaultValue = "0") Integer ball) {
//		ArrayList<Person> listPerson = getPersons();
//		ArrayList<Person> listPersonFilter = new ArrayList<Person>();
//
//		if (type < 4 && player == 0) {// chỉ nhập type
//			for (Person person : listPerson) {
//				if ((type == 1 && person.getPersonType() == PersonType.WORKER)
//						|| (type == 2 && person.getPersonType() == PersonType.STUDENT)
//						|| (type == 3 && person.getPersonType() == PersonType.PROFESSOR)) {
//					listPersonFilter.add(person);
//				} else if (type == 0) {
//					listPersonFilter.add(person);
//				}
//			}
//			System.out.println(listPersonFilter.size());
//		} else if (type < 4 || type == 0 && player != 0) { // nhập type từ 0 - 3 và nhập player 0 - 3
//			System.out.println("hello2");
//			for (int i = 0; i < listPerson.size(); i++) {
//				if (listPerson.get(i).getListPlay() != null) {
//					for (IPlayer elementPlayer : listPerson.get(i).getListPlay()) {
//						if (player == 1 && elementPlayer instanceof CDPlayer
//								|| player == 2 && elementPlayer instanceof TapePlayer
//								|| player == 3 && elementPlayer instanceof DVDPlayer) {
//							listPersonFilter.add(listPerson.get(i));
//						}
//					}
//				}
//			}
//			System.out.println(listPersonFilter.size());
//		} else if (player == 0 || player >= 4 && type >= 4 || type == 0) { // không nhập type, hoặc nhập type sai, không
//																			// nhập player hoặc nhập sai player
//			System.out.println("hello3");
//			listPersonFilter = listPerson;
//			System.out.println(listPersonFilter.size());
//		}
//
//		if ((type == 0 || player == 0) && ball == 0) {
//			listPersonFilter = listPerson;
//		} else if ((type == 0 || player == 0) && ball != 0) {
//			System.out.println("hello 5");
//			for (Person person : listPerson) {
//				if (person.getListBall() != null) {
//					for (Ball ballElement : person.getListBall()) {
//						if (ball == 1 && ballElement instanceof BaseBall
//								|| ball == 2 && ballElement instanceof FootBall) {
//							listPersonFilter.add(person);
//						}
//					}
//				}
//			}
//		} else if((type == 0 || player == 0)) {
//			
//		}
//
//		return listPersonFilter;
//	}

	@CrossOrigin
	@GetMapping("/listPerson") // 1-worker, 2-student, 3-professor
	public ArrayList<Person> getListPerson(@RequestParam(value = "type", defaultValue = "0") Integer type,
			@RequestParam(value = "player", defaultValue = "0") Integer player,
			@RequestParam(name = "ball", defaultValue = "0") Integer ball) {
		ArrayList<Person> listPerson = getPersons();
		ArrayList<Person> listPersonFilter = new ArrayList<Person>();

		if (type == 0 || player == 0 || ball == 0) {
			listPersonFilter = listPerson;
		}

		if (type < 4 && type != 0 || player < 4 && player != 0 || ball < 3 && ball != 0) {
			System.out.println("hello");
			for (Person person : listPerson) {
				if ((type == 1 && person.getPersonType() == PersonType.WORKER)
						|| (type == 2 && person.getPersonType() == PersonType.STUDENT)
						|| (type == 3 && person.getPersonType() == PersonType.PROFESSOR)) {
					if (person.getListPlay() != null) {
						for (IPlayer playerElement : person.getListPlay()) {
							if ((playerElement != null) && (player == 1 && playerElement instanceof CDPlayer
									|| player == 2 && playerElement instanceof TapePlayer
									|| player == 3 && playerElement instanceof DVDPlayer)) {
								for (Ball ballElement : person.getListBall()) {
									if (ballElement != null && ((ball == 1 && ballElement instanceof BaseBall)
											|| (ball == 2 && ballElement instanceof FootBall))) {
										listPersonFilter.add(person);
									} else if(ballElement == null) {
										listPersonFilter.add(person);
									}
								}
							} else if(playerElement == null) {
								listPersonFilter.add(person);
							}
						}
					} else if(person.getListPlay() == null) {
						listPersonFilter.add(person);
					}
				}
			}
		}
		return listPersonFilter;
	}

}
