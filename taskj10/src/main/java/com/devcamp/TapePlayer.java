package com.devcamp;

public class TapePlayer implements IRecorder {
	private String playerType;

	public TapePlayer(String playerType) {
		super();
		this.playerType = playerType;
	}

	@Override
	public String getPlay() {
		return "play";
	}

	@Override
	public String getStop() {
		return "stop";
	}

	@Override
	public String getPause() {
		return "pause";
	}

	@Override
	public String getReverse() {
		return "reverse";
	}

	@Override
	public String getRecord() {
		return "get records";
	}

	public String getPlayerType() {
		return playerType;
	}

	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}

}
