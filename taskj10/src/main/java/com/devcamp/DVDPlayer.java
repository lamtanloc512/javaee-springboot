package com.devcamp;

public class DVDPlayer implements IPlayer {
	private String playerType;

	public DVDPlayer(String playerType) {
		super();
		this.playerType = playerType;
	}

	@Override
	public String getPlay() {
		return "play";
	}

	@Override
	public String getStop() {
		return "stop";
	}

	@Override
	public String getPause() {
		return "pause";
	}

	@Override
	public String getReverse() {
		return "reverse";
	}

	public String getPlayerType() {
		return playerType;
	}

	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}

}
