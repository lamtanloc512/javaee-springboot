package com.devcamp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CDrink;
import com.devcamp.repository.IDrinkRespository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CDrinkController {

    @Autowired
    IDrinkRespository drinkRespository;

    @GetMapping(path = "/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
	try {
	    return new ResponseEntity<>(drinkRespository.findAll(), HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<Optional<CDrink>> getDrinkById(@PathVariable("id") long id) {
	Optional<CDrink> drinkById = drinkRespository.getById(id);
	if (drinkById.isPresent()) {
	    return new ResponseEntity<>(drinkById, HttpStatus.OK);
	} else {
	    return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
    }

    @PostMapping("/drinks")
    public ResponseEntity<CDrink> createCDrink(@Valid @RequestBody CDrink pDrink) {
	try {
	    pDrink.setNgayTao(new Date());
	    pDrink.setNgayCapNhat(null);
	    CDrink _drink = drinkRespository.save(pDrink);
	    return new ResponseEntity<>(_drink, HttpStatus.CREATED);
	} catch (Exception e) {
	    System.out.println(e);
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<CDrink> updateCVoucherById(@Valid @PathVariable("id") long id, @RequestBody CDrink pDrink) {
	// TODO: Hãy viết code lấy voucher từ DB để UPDATE
	Optional<CDrink> drinkData = drinkRespository.findById(id);
	if (drinkData.isPresent()) {
	    CDrink drink = drinkData.get();
	    drink.setDonGia(pDrink.getDonGia());
	    drink.setMaNuocUong(pDrink.getMaNuocUong());
	    drink.setTenNuocUong(pDrink.getTenNuocUong());
	    drink.setGhiChu(pDrink.getGhiChu());
	    drink.setNgayCapNhat(new Date());

	    try {
		return new ResponseEntity<>(drinkRespository.save(pDrink), HttpStatus.OK);
	    } catch (Exception e) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	} else {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

    @DeleteMapping("/drinks")
    public ResponseEntity<Object> deleteDrink() {
	try {
	    drinkRespository.deleteAll();
	    return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	} catch (Exception e) {
	    return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
	}
    }

}
