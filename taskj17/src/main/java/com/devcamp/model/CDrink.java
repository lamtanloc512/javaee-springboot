package com.devcamp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Table(name = "drinks")
@Data
public class CDrink {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long drink_id;
    
    @NotNull(message = "Phải có mã giảm giá")
    @Size(min = 3, message = "Mã voucher phải có ít nhất 3 ký tự ")
    @Column(name = "ma_nuoc_uong")
    private String maNuocUong;

    @Column(name = "ten_nuoc_uong")
    private String tenNuocUong;
    
    @NotEmpty(message = "Nhập đơn giá")
    @Range(min=10000, max=500000, message = "Nhập giá trị từ 10k đến 500k")
    @Column(name = "don_gia")
    private String donGia;

    @Column(name = "ghi_chu")
    private String ghiChu;
    
    
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    private Date ngayTao;
    
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_cap_nhat", nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayCapNhat;
}
