package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Taskj17Application {

	public static void main(String[] args) {
		SpringApplication.run(Taskj17Application.class, args);
	}

}
