package com.devcamp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

    Optional<CVoucher> findById(long id);

}
