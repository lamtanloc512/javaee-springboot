package com.devcamp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CDrink;

public interface IDrinkRespository extends JpaRepository<CDrink, Long>{
    Optional<CDrink> getById(long id);
    Optional<CDrink> findById(long id);
}
