package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskJ11Application {

	public static void main(String[] args) {
		SpringApplication.run(TaskJ11Application.class, args);
	}

}
