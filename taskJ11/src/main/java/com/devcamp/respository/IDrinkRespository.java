package com.devcamp.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CDrink;

public interface IDrinkRespository extends JpaRepository<CDrink, Long> {
	
}
