package com.devcamp.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CVoucher;

public interface IVoucherRespository extends JpaRepository<CVoucher, Long> {

}
