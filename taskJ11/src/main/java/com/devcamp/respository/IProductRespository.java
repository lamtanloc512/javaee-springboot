package com.devcamp.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.model.CProduct;

public interface IProductRespository extends JpaRepository<CProduct, Long> {

}
