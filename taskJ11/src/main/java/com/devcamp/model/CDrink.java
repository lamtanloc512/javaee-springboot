package com.devcamp.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "drinks")
public class CDrink {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "ma_nuoc_uong")
	private String maNuocUong;
	
	@Column(name = "ten_nuoc_uong")
	private String tenNuocUong;
	
	@Column(name = "gia_nuoc_uong")
	private long price;
	
	@Column(name = "ngay_tao")
	private LocalDate ngayTao;
	
	@Column(name = "ngay_cap_nhat")
	private LocalDate ngayCapNhat;

	public CDrink() {
	}

	public CDrink(int id, String maNuocUong, String tenNuocUong, long price, LocalDate ngayTao, LocalDate ngayCapNhat) {
		super();
		this.id = id;
		this.maNuocUong = maNuocUong;
		this.tenNuocUong = tenNuocUong;
		this.price = price;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMaNuocUong() {
		return maNuocUong;
	}

	public void setMaNuocUong(String maNuocUong) {
		this.maNuocUong = maNuocUong;
	}

	public String getTenNuocUong() {
		return tenNuocUong;
	}

	public void setTenNuocUong(String tenNuocUong) {
		this.tenNuocUong = tenNuocUong;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public LocalDate getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(LocalDate ngayTao) {
		this.ngayTao = ngayTao;
	}

	public LocalDate getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(LocalDate ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

}