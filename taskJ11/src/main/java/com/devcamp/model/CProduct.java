package com.devcamp.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class CProduct {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long productId;

	@Column(name = "product_name")
	private String productName;

	@Column(name = "product_code")
	private int productCode;

	@Column(name = "product_price")
	private int price;

	@Column(name = "date_created")
	private LocalDate ngayTao;

	@Column(name = "date_updated")
	private LocalDate ngayCapNhat;

	public CProduct(long productId, String productName, int productCode, int price, LocalDate ngayTao,
			LocalDate ngayCapNhat) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productCode = productCode;
		this.price = price;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
	}

	public CProduct() {
		super();
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductCode() {
		return productCode;
	}

	public void setProductCode(int productCode) {
		this.productCode = productCode;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public LocalDate getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(LocalDate ngayTao) {
		this.ngayTao = ngayTao;
	}

	public LocalDate getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(LocalDate ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

}
