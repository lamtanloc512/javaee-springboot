package com.devcamp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class CCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	private String email;
	private String phoneNumber;
	private String address;
	private Date ngayTao;
	private Date ngayCapNhat;
	
	public CCustomer() {
	}
	
	public CCustomer(int id, String name, String email, String phoneNumber, String address, Date ngayTao,
	                 Date ngayCapNhat) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Date getNgayTao() {
		return ngayTao;
	}
	
	public void setNgayTao(Date ngayTao) {
		this.ngayTao = ngayTao;
	}
	
	public Date getNgayCapNhat() {
		return ngayCapNhat;
	}
	
	public void setNgayCapNhat(Date ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}
}
