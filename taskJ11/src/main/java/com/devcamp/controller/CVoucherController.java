package com.devcamp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CVoucher;
import com.devcamp.respository.IVoucherRespository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CVoucherController {

	@Autowired
	IVoucherRespository voucherRespository;

	@GetMapping("/vouchers")
	public List<CVoucher> getVouchers() {
		List<CVoucher> listVouchers = new ArrayList<CVoucher>();
		voucherRespository.findAll().forEach(listVouchers::add);
		return listVouchers;
	}

	@GetMapping("/vouchers2")
	public ResponseEntity<List<CVoucher>> getVouchers2() {
		try {
			List<CVoucher> listVouchers = new ArrayList<CVoucher>();
			voucherRespository.findAll().forEach(listVouchers::add);
			return new ResponseEntity<>(listVouchers, HttpStatus.FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
