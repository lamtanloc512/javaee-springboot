package com.devcamp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CProduct;
import com.devcamp.respository.IProductRespository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
public class CProductController {
	@Autowired
	IProductRespository productRespository;

	@GetMapping("/products")
	public ResponseEntity<List<CProduct>> getProducts() {
		try {
			List<CProduct> listVouchers = new ArrayList<CProduct>();
			productRespository.findAll().forEach(listVouchers::add);
			return new ResponseEntity<>(listVouchers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
