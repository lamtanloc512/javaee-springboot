package com.devcamp.model;

public class CFish extends CAnimal {
	private int size;
	private boolean canEat;

	public CFish() {
		super();
	}

	public CFish(int id, String gender) {
		super(id, gender);
	}

	@Override
	public void isMamal() {
		super.isMamal();
	}

	@Override
	public void mate() {
		super.mate();
	}

	@Override
	public void swim() {
		super.swim();
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isCanEat() {
		return canEat;
	}

	public void setCanEat(boolean canEat) {
		this.canEat = canEat;
	}

	@Override
	public String animalSound() {
		return new String("no sound ...");
	}

	@Override
	public String sleep() {
		return new String("no sleep ...");
	}
	
	

}
