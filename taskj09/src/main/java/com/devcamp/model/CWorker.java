package com.devcamp.model;

import java.util.Map;

public class CWorker extends CPerson {
	private int salary;

	public CWorker() {
		super();
	}

	public CWorker(int age, String name, String gender, CAddress address, Map<String, Object> listPet,int salary) {
		super(age, name, gender, address, listPet);
		this.salary = salary;
	}

	@Override
	public String eat() {
		return "eat bread ...";
	}

	public String working() {
		return new String("Working ...");
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public String gotoShop() {
		return new String("go to shop ...");
	}

	@Override
	public String play() {
		return new String("go to play ...");
	}
	
	

}
