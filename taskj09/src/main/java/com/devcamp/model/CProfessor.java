package com.devcamp.model;

import java.util.Map;

public class CProfessor extends CPerson implements ILive, ISchool{
	private int salary;
	
	
	
	public CProfessor() {
	}

	public CProfessor(int age, String name, String gender, CAddress address, Map<String, Object> listPet) {
		super(age, name, gender, address, listPet);
	}

	public CProfessor(int salary) {
		super();
		this.salary = salary;
	}

	@Override
	public String eat() {
		return "eat pizza ...";
	}
	
	public String teaching() {
		return new String("Teaching ...");
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public String gotoSchool() {
		return new String("go to school ...");
	}

	
	
}
