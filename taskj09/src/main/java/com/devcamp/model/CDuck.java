package com.devcamp.model;

import org.springframework.stereotype.Component;

@Component
public class CDuck extends CAnimal {
	private String beakColor;

	public CDuck() {
		super();
	}

	public CDuck(int id, String gender) {
		super(id, gender);
	}

	@Override
	public void isMamal() {
		super.isMamal();
	}

	@Override
	public void mate() {
		super.mate();
	}

	@Override
	public void swim() {
		super.swim();
	}

	public void quack() {
		System.out.println("quack quack ...");
	}

	public String getBeakColor() {
		return beakColor;
	}

	public void setBeakColor(String beakColor) {
		this.beakColor = beakColor;
	}

	@Override
	public String animalSound() {
		return new String("quack quack ...");
	}

	@Override
	public String sleep() {
		return new String("zzzz ...");
	}
	
	
}
