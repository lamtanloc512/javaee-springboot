package com.devcamp.model;

import java.util.ArrayList;
import java.util.Map;

public class CStudent extends CPerson implements ISchool {
	private int studentId;
	private float averageMark;
	private ArrayList<CSubject> subjects;
	

	public CStudent() {
	}

	public CStudent(int age, String name, String gender, CAddress address, Map<String, Object> listPet) {
		super(age, name, gender, address, listPet);
	}

	public CStudent(int studentId, float averageMark) {
		super();
		this.studentId = studentId;
		this.averageMark = averageMark;
	}

	public void doHomeWork() {

	}

	@Override
	public String eat() {
		return new String("eat noodle ...");
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public float getAverageMark() {
		return averageMark;
	}

	public void setAverageMark(float averageMark) {
		this.averageMark = averageMark;
	}

	public ArrayList<CSubject> getSubjects() {
		return subjects;
	}

	public void setSubjects(ArrayList<CSubject> subjects) {
		this.subjects = subjects;
	}

	@Override
	public String gotoSchool() {
		return new String("go to school ...");
	}

	
	
}
