package com.devcamp.model;

import java.util.Map;

public abstract class CPerson implements ILive, IAnimal {
	private int age;
	private String name;
	private String gender;
	private CAddress address;
	private Map<String, Object> listPet;

	public CPerson() {
		super();
	}

	public CPerson(int age, String name, String gender, CAddress address, Map<String, Object> listPet) {
		super();
		this.age = age;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.listPet = listPet;
	}

	public abstract String eat();

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public CAddress getAddress() {
		return address;
	}

	public void setAddress(CAddress address) {
		this.address = address;
	}

	public Map<String, Object> getListPet() {
		return listPet;
	}

	public void setListPet(Map<String, Object> map) {
		this.listPet = map;
	}

	@Override
	public String toString() {
		return "CPerson [age=" + age + ", name=" + name + ", gender=" + gender + ", address=" + address + ", listPet="
				+ listPet + "]";
	}

	@Override
	public String gotoShop() {
		return null;
	}

	@Override
	public String play() {
		return null;		
	}

	@Override
	public String animalSound() {
		return new String("Latin...");
	}

	@Override
	public String sleep() {
		return new String("zzzz...");
	}
	
	
	
	
}
