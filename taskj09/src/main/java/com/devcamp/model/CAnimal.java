package com.devcamp.model;

public abstract class CAnimal implements IAnimal {
	private int id;
	private String gender;

	public CAnimal() {

	}

	public CAnimal(int id, String gender) {
		super();
		this.id = id;
		this.gender = gender;
	}

	public void isMamal() {
		System.out.println("is mamal ...");
	}

	public void mate() {
		System.out.println("get mate ...");
	}

	public void swim() {
		System.out.println("swim ...");
	}

	// get set

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String animalSound() {
		return null;
	}

	@Override
	public String sleep() {
		return null;
	}

	
}
