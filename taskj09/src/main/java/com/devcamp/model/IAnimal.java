package com.devcamp.model;

public interface IAnimal {
	String animalSound();
	String sleep();
}
