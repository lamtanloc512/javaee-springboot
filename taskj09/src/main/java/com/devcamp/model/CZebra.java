package com.devcamp.model;

public class CZebra extends CAnimal {
	private boolean is_wild;

	public CZebra() {
		super();
	}

	public CZebra(int id, String gender) {
		super(id, gender);
	}

	@Override
	public void isMamal() {
		super.isMamal();
	}

	@Override
	public void mate() {
		super.mate();
	}

	public void run() {
		System.out.println("run...");
	}

	public boolean isIs_wild() {
		return is_wild;
	}

	public void setIs_wild(boolean is_wild) {
		this.is_wild = is_wild;
	}

	@Override
	public String animalSound() {
		return new String("ohm ohm ...");
	}

	@Override
	public String sleep() {
		return new String("zzzz ...");
	}
	
	

}
