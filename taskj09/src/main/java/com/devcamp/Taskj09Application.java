package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Taskj09Application {

	public static void main(String[] args) {
		SpringApplication.run(Taskj09Application.class, args);
	}

}
