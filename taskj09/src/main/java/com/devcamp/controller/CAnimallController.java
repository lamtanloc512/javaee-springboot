package com.devcamp.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.service.CAnimalservice;

@RestController
public class CAnimallController {
	
	@Autowired
	CAnimalservice animalList;
	
	@GetMapping("/")
	public Map<String, Object> testing() {
		return animalList.getListAnimals();
	}
}
