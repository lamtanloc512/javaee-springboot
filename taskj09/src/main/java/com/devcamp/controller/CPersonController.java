package com.devcamp.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.CPerson;
import com.devcamp.service.CPersonService;

@RestController
public class CPersonController {
	@Autowired
	CPersonService personService;

	@GetMapping("/students")
	public List<CPerson> getAllCPersons() {
		return personService.getAllCStudents();
	}

	@GetMapping("/workers")
	public List<CPerson> getAllWorkers() {
		return personService.getAllCWorkers();
	}

	@GetMapping("/professors")
	public List<CPerson> getAllProfessors() {
		return personService.getAllCProfessors();
	}

	@GetMapping("/all")
	public Map<String, Object> getAll(@RequestParam(name = "type", defaultValue = "all") String type) {
		Map<String, Object> results = new HashMap<String, Object>();

		switch (type) {
		case "professors":
			results.clear();
			results.put("professors", personService.getAllCProfessors());
			break;
		case "workers":
			results.clear();
			results.put("workers", personService.getAllCWorkers());
			break;
		case "students":
			results.clear();
			results.put("students", personService.getAllCStudents());
			break;
		default:
			results.clear();
			results.put("all", personService.getAll());
			break;
		}

		return results;
	}
}
