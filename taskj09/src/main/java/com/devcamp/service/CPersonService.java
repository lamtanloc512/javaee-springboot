package com.devcamp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.devcamp.model.CAddress;
import com.devcamp.model.CPerson;
import com.devcamp.model.CProfessor;
import com.devcamp.model.CStudent;
import com.devcamp.model.CWorker;

@Service
public class CPersonService {
	private static List<CProfessor> teacherList1 = new ArrayList<CProfessor>();
	private static List<CProfessor> teacherList2 = new ArrayList<CProfessor>();
	private static List<CProfessor> teacherList3 = new ArrayList<CProfessor>();
	private static List<CPerson> studentList = new ArrayList<CPerson>();
	private static List<CPerson> workerList = new ArrayList<CPerson>();
	private static List<CPerson> professorList = new ArrayList<CPerson>();
	private static Map<String, Object> all = new HashMap<String, Object>();

	static CAnimalservice petList = new CAnimalservice();

	static {
		CProfessor peter = new CProfessor(50, "Peter Parker", "male", null, null);
		CProfessor marry = new CProfessor(51, "Marry", "female", null, null);
		CProfessor andrew = new CProfessor(52, "Andrew", "male", null, null);
		CProfessor tom = new CProfessor(53, "Tom", "male", null, null);

		teacherList1.add(peter);
		teacherList1.add(marry);
		teacherList2.add(andrew);
		teacherList2.add(marry);
		teacherList3.add(tom);
		teacherList3.add(andrew);

		CPerson student1 = new CStudent(18, "Andrei", "male", null, null);
		CPerson student2 = new CStudent(19, "Maria", "female", null, null);
		CPerson student3 = new CStudent(21, "Natasha", "female", null, null);
		CPerson student4 = new CStudent(17, "Julia", "female", null, null);
		CPerson worker1 = new CWorker(17, "Cook", "female", null, null, 20);
		CPerson worker2 = new CWorker(17, "Votong", "male", null, null, 40);
		CPerson worker3 = new CWorker(17, "Tim", "male", null, null, 50);

		CAddress address1 = new CAddress("123", "NewYork", "US", 178);
		CAddress address2 = new CAddress("321", "Florida", "US", 585);
		CAddress address3 = new CAddress("456", "Washington DC", "US", 541);
		CAddress address4 = new CAddress("789", "Texas", "US", 312);
		CAddress address5 = new CAddress("457645", "Texas", "US", 546);
		CAddress address6 = new CAddress("781219", "Texas", "US", 332);
		CAddress address7 = new CAddress("78923", "Texas", "US", 111);
		CAddress address8 = new CAddress("111234", "Texas", "US", 223);
		CAddress address9 = new CAddress("111234", "Texas", "US", 223);
		CAddress address10 = new CAddress("11124234", "Texas", "US", 544);
		CAddress address11 = new CAddress("12312445", "Texas", "US", 445);

		peter.setListPet(petList.getListAnimals());
		peter.setAddress(address1);
		marry.setListPet(petList.getListAnimals());
		marry.setAddress(address2);
		andrew.setListPet(petList.getListAnimals());
		andrew.setAddress(address3);
		tom.setListPet(petList.getListAnimals());
		tom.setAddress(address4);

		student1.setListPet(petList.getListAnimals());
		student1.setAddress(address5);
		student2.setListPet(petList.getListAnimals());
		student2.setAddress(address6);
		student3.setListPet(petList.getListAnimals());
		student3.setAddress(address7);
		student4.setListPet(petList.getListAnimals());
		student4.setAddress(address8);
		worker1.setListPet(petList.getListAnimals());
		worker1.setAddress(address9);
		worker2.setListPet(petList.getListAnimals());
		worker2.setAddress(address10);
		worker3.setListPet(petList.getListAnimals());
		worker3.setAddress(address11);
		
		studentList.add(student1);
		studentList.add(student2);
		studentList.add(student3);
		studentList.add(student4);
		workerList.add(worker1);
		workerList.add(worker2);
		workerList.add(worker3);
		
		professorList.add(peter);
		professorList.add(marry);
		professorList.add(andrew);
		professorList.add(tom);
		
		all.put("professors", professorList);
		all.put("workers", workerList);
		all.put("students", studentList);
		
	}

	public CPersonService instance() {
		return new CPersonService();
	}

	public List<CPerson> getAllCStudents() {
		return studentList;
	}
	public List<CPerson> getAllCWorkers() {
		return workerList;
	}
	public List<CPerson> getAllCProfessors() {
		return professorList;
	}
	public Map<String, Object> getAll() {
		return all;
	}

}
