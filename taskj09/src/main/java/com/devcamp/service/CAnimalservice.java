package com.devcamp.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.devcamp.model.CAnimal;
import com.devcamp.model.CDuck;
import com.devcamp.model.CFish;
import com.devcamp.model.CZebra;

@Service
public class CAnimalservice {
	private static Map<String, Object> list = new HashMap<String, Object>();
	static {
		Map<String, Object> animals = new HashMap<String, Object>();
		CAnimal duck = new CDuck(1, "male"); 
		((CDuck) duck).setBeakColor("white");
		
		CAnimal fish = new CFish(2, "female"); 
		
		((CFish) fish).setSize(4);
		((CFish) fish).setCanEat(true);
		
		
		CAnimal zebra = new CZebra(3, "male"); 
		((CZebra) zebra).setIs_wild(true);
		
		
		animals.put("duck",  duck);
		animals.put("fish", fish);
		animals.put("zebra", zebra);
		list = animals;
	}

	
	public Map<String, Object> getListAnimals(){
		return list;
	}

}
