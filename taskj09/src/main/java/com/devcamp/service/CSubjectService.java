package com.devcamp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.devcamp.model.CSubject;

public class CSubjectService {
	private static List<CSubject> subject1 = new ArrayList<CSubject>();
	private static List<CSubject> subject2 = new ArrayList<CSubject>();
	private static List<CSubject> subject3 = new ArrayList<CSubject>();

	static {

		CSubject toan = new CSubject("Toán", 1, null);
		CSubject nguvan = new CSubject("Ngữ Văn", 2, null);
		CSubject vatly = new CSubject("Vật Lý", 3, null);
		CSubject tienganh = new CSubject("Tiếng Anh", 4, null);
		subject1.add(toan);
		subject1.add(tienganh);
		subject2.add(vatly);
		subject2.add(nguvan);
		subject3.add(toan);
		subject3.add(vatly);

	}

	public Map<String, Object> getAllCSubjects() {
		Map<String, Object> allSubject = new HashMap<String, Object>();

		allSubject.put("subjectList1", subject1);
		allSubject.put("subjectList2", subject2);
		allSubject.put("subjectList3", subject3);

		return allSubject;
	}

	public static List<CSubject> getSubject1() {
		return subject1;
	}

	public static void setSubject1(List<CSubject> subject1) {
		CSubjectService.subject1 = subject1;
	}

	public static List<CSubject> getSubject2() {
		return subject2;
	}

	public static void setSubject2(List<CSubject> subject2) {
		CSubjectService.subject2 = subject2;
	}

	public static List<CSubject> getSubject3() {
		return subject3;
	}

	public static void setSubject3(List<CSubject> subject3) {
		CSubjectService.subject3 = subject3;
	}

}
