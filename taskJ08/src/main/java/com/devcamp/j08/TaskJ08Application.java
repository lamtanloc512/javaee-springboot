package com.devcamp.j08;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskJ08Application {

	public static void main(String[] args) {
		SpringApplication.run(TaskJ08Application.class, args);
	}

}
