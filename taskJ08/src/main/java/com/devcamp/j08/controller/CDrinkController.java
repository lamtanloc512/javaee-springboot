package com.devcamp.j08.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j08.model.CDrink;
import com.devcamp.j08.service.CDrinkService;

@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/app")
@RestController
public class CDrinkController {

	@Autowired
	CDrinkService cdrinks;

	@GetMapping("/drinks")
	public List<CDrink> getAllDrinkList() {
		cdrinks.createDrinkOption();
		return cdrinks.getDrinks();
	}

	@PostMapping("/drinks")
	public ResponseEntity<CDrink> createNewDrink(@RequestBody CDrink newDrink) {
		cdrinks.getDrinks().add(newDrink);
		return ResponseEntity.ok(newDrink);
	}

	@PutMapping("/drinks/id/{id}")
	public CDrink updateDrinkOpt(@RequestBody CDrink editDrink, @PathVariable int id) {
		int i;
		try {
			i = cdrinks.findById(id);
			cdrinks.getDrinks().set(i, editDrink);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return editDrink;
	}
	
	@DeleteMapping("/drinks/id/{id}")
	public String deleteDrinkOpt(@PathVariable int id) throws Exception {
		try {
			int i = cdrinks.findById(id);
			cdrinks.getDrinks().remove(i);
			return "Bạn đã xóa thành công";
		} catch (Exception e) {
			return "Xóa không thành công";
		}
		
	}
	
}
