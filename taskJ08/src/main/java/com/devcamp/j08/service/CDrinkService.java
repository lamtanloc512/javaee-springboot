package com.devcamp.j08.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.j08.model.CDrink;

@Service
public class CDrinkService {
	private static List<CDrink> drinks = new ArrayList<CDrink>();

	static {
		LocalDate ngayTao = LocalDate.now();
		CDrink tratac = new CDrink(1, "TRATAC", "Trà Tắc", 10000, ngayTao, ngayTao);
		CDrink coca = new CDrink(2, "COCA", "Cocacola", 15000, ngayTao, ngayTao);
		CDrink pepsi = new CDrink(3, "PEPSI", "Pepsi", 15000, ngayTao, ngayTao);
		try {
			drinks.add(tratac);
			drinks.add(coca);
			drinks.add(pepsi);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createDrinkOption() {

	}

	public List<CDrink> getDrinks() {
		return drinks;
	}

	public void setDrinks(List<CDrink> drinks) {
		CDrinkService.drinks = drinks;
	}

	public int findById(int id) {
		int indexOfId = 0;
		for (int i = 0; i < drinks.size(); i++) {
			if (drinks.get(i).getId() == id) {
				indexOfId = i;
			} else if (drinks.get(i).getId() != id) {
				indexOfId = id;
			}
		}
		return indexOfId;
	}
}
